import torch.nn as nn
import torch
# 
n_classes = 60
class NetG(nn.Module):
    def __init__(self, ngf, nz):
        super(NetG, self).__init__()
        #plus
        self.label_emb = nn.Embedding(n_classes, 10)
        #plusend
        self.layer1_1 = nn.Sequential(nn.ConvTranspose2d(nz, ngf * 4, kernel_size=4, stride=1, padding=0, bias=False),
                    nn.BatchNorm2d(ngf * 4),
                    nn.ReLU(inplace = True)
                    )
        self.layer1_2 = nn.Sequential(nn.ConvTranspose2d(n_classes, ngf * 4, kernel_size=4, stride=1, padding=0, bias=False),
                    nn.BatchNorm2d(ngf * 4),
                    nn.ReLU(inplace = True)
                    ) 
        self.layer2 = nn.Sequential(nn.ConvTranspose2d(ngf * 8, ngf * 4, 4, 2, 1, bias=False),
                    nn.BatchNorm2d(ngf * 4),
                    nn.ReLU(inplace = True)
                    )         
        self.layer3 = nn.Sequential(nn.ConvTranspose2d(ngf * 4, ngf * 2, 4, 2, 1, bias=False),
                    nn.BatchNorm2d(ngf * 2),
                    nn.ReLU(inplace = True)
                    )
        self.layer4 = nn.Sequential(nn.ConvTranspose2d(ngf * 2, ngf, 4, 2, 1, bias=False),
                    nn.BatchNorm2d(ngf),
                    nn.ReLU(inplace = True)
                    )
        self.layer5 = nn.Sequential(
            nn.ConvTranspose2d(ngf , 3, 5, 3, 1, bias=False),
            nn.Tanh()
        )                       
                    
        #change

        #self.deconv1_1 = nn.ConvTranspose2d(nz, ngf * 8, kernel_size=4, stride=1, padding=0, bias=False)
        #self.deconv1_1_bn = nn.BatchNorm2d(ngf * 8) 
        #self.deconv1_2 = nn.ConvTranspose2d(15, ngf * 8, kernel_size=4, stride=1, padding=0, bias=False)
        #self.deconv1_2_bn = nn.BatchNorm2d(ngf * 8)
        #self.deconv2 = nn.ConvTranspose2d(ngf * 8, ngf * 4, 4, 2, 1, bias=False)
        #self.deconv2_bn = nn.BatchNorm2d(ngf * 4)
        #self.deconv3 = nn.ConvTranspose2d(ngf * 4, ngf * 2, 4, 2, 1, bias=False)
        #self.deconv3_bn = nn.BatchNorm2d(ngf * 2)
        #self.deconv4 = nn.ConvTranspose2d(ngf * 2, ngf, 4, 2, 1, bias=False)
        #self.deconv4_bn = nn.BatchNorm2d(ngf)
        #self.deconv5 = nn.ConvTranspose2d(ngf, 3, 5, 3, 1, bias=False)
    
    def forward(self, x, label):
        
        out = self.layer1_1(x)
        #print(out.shape)
        out_label = self.layer1_2(label)
        #print(out_label.shape)
        out = torch.cat((out,out_label) , 1 )
        #print(out.shape)
        out = self.layer2(out)
        #print(out.shape)
        out = self.layer3(out)
        #print(out.shape)
        out = self.layer4(out)
        #print(out.shape)
        out = self.layer5(out)
        #print(out.shape)
        return out  
        #print(out.size()) 
        #print(out_label.size())
        #out = nn.ReLu(self.deconv1_1_bn(self.deconv1_1(x)))
        #out_label = nn.ReLu(self.deconv1_2_bn(self.deconv1_2(self.label.emb(label)))
        #out = torch.cat( ( out , out_label ) , 0 )
        #out = nn.relu(self.deconv2_bn(self.deconv2(out)))
        #out = nn.relu(self.deconv3_bn(self.deconv3(out)))
        #out = nn.relu(self.deconv4_bn(self.deconv4(out)))
        #out = nn.Tanh(self.deconv5(out))
        #return out
        
        
        
        
#        self.layer1 = nn.Sequential(
#            nn.ConvTranspose2d(nz, ngf * 8, kernel_size=4, stride=1, padding=0, bias=False),
#            nn.BatchNorm2d(ngf * 8),
#            nn.ReLU(inplace=True)
#        )
        # 
#        self.layer2 = nn.Sequential(
#            nn.ConvTranspose2d(ngf * 8, ngf * 4, 4, 2, 1, bias=False),
#            nn.BatchNorm2d(ngf * 4),
#            nn.ReLU(inplace=True)
#        )
        # 
#        self.layer3 = nn.Sequential(
#            nn.ConvTranspose2d(ngf * 4, ngf * 2, 4, 2, 1, bias=False),
#            nn.BatchNorm2d(ngf * 2),
#            nn.ReLU(inplace=True)
#        )
        # 
#        self.layer4 = nn.Sequential(
#            nn.ConvTranspose2d(ngf * 2, ngf, 4, 2, 1, bias=False),
#            nn.BatchNorm2d(ngf),
#            nn.ReLU(inplace=True)
#       )
        # 
#        self.layer5 = nn.Sequential(
#            nn.ConvTranspose2d(ngf, 3, 5, 3, 1, bias=False),
#            nn.Tanh()
#        )

    # 
#    def forward(self, x):
#        out = self.layer1(x)
#        out = self.layer2(out)
#        out = self.layer3(out)
#        out = self.layer4(out)
#        out = self.layer5(out)
#        return out


# 
class NetD(nn.Module):
    def __init__(self, ndf):
        super(NetD, self).__init__()
        # 
    #    self.conv1_1 = nn.Conv2d(3, ndf, kernel_size=5, stride=3, padding=1, bias=False)
    #    self.conv1_2 = nn.Conv2d(15, ndf, kernel_size=5, stride=3, padding=1, bias=False)
        #self.conv1_2 = nn.Conv2d(15, kernel_size=5, stride=3, padding=1, bias=False)       
   #     self.conv2 = nn.Conv2d(ndf, ndf * 2, 4, 2, 1, bias=False)
   #     self.conv2_bn = nn.BatchNorm2d(ndf * 2)
  #      self.conv3 = nn.Conv2d(ndf * 2, ndf * 4, 4, 2, 1, bias=False)
  #      self.conv3_bn = nn.BatchNorm2d(ndf * 4)
  #      self.conv4 = nn.Conv2d(ndf * 4, ndf * 8, 4, 2, 1, bias=False)
  #      self.conv4_bn = nn.BatchNorm2d(ndf * 8)
 #       self.conv5 = nn.Conv2d(ndf * 8, 1, 4, 1, 0, bias=False)
        
#    def forward(self, x, label):
#        out = nn.LeakyReLU(self.conv1_1(x), 0.2)
#        out_label = nn.LeakyReLU(self.conv1_2(label), 0.2)
#        out = torch.cat( (out , out_label ) , 1)
#        out = nn.LeakyReLU(self.conv2_bn(self.conv2(out)), 0.2)
#        out = nn.LeakyReLU(self.conv3_bn(self.conv3(out)), 0.2)
#        out = nn.LeakyReLU(self.conv4_bn(self.conv4(out)), 0.2)
#        out = nn.Sigmoid(self.conv5(out))
#        return out
        
        
        self.layer1_1 = nn.Sequential(
            nn.Conv2d(3, ndf//2, kernel_size=4, stride=3, padding=1, bias=False),
            nn.LeakyReLU(0.2, inplace=True)
        )
        self.layer1_2 = nn.Sequential(
            nn.Conv2d(n_classes, ndf//2, kernel_size=4, stride=3, padding=1, bias=False),
            nn.LeakyReLU(0.2, inplace=True)
        )
      
         
        self.layer2 = nn.Sequential(
            nn.Conv2d(ndf*1, ndf *2, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf *2),
            nn.LeakyReLU(0.2, inplace=True)
       )
        # 
        self.layer3 = nn.Sequential(
            nn.Conv2d(ndf * 2, ndf * 4, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 4),
            nn.LeakyReLU(0.2, inplace=True)
        )
        # 
        self.layer4 = nn.Sequential(
            nn.Conv2d(ndf * 4, ndf * 8, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 8),
            nn.LeakyReLU(0.2, inplace=True)
        )
         
        self.layer5 = nn.Sequential(
            nn.Conv2d(ndf * 8, 1, 4, 1, 0, bias=False),
            nn.Sigmoid()
        )
        #self.layer4 = nn.Sequential(
            #nn.Conv2d(ndf * 8, 1, 4, 1, 0, bias=False),
            #nn.Sigmoid()
        #)        
    def forward(self, x, label):
        
        out = self.layer1_1(x)
        #print(out.shape)
        out_label = self.layer1_2(label)
        #print(out_label.shape)
        out = torch.cat((out,out_label) , 1 )
        #print(out.shape)
        out = self.layer2(out)
        #print(out.shape)
        out = self.layer3(out)
        #print(out.shape)
        out = self.layer4(out)
        #print(out.shape)
        out = self.layer5(out)
        #print(out.shape)        
        #out = self.layer5(out)
        #print(out.shape)
        return out

    
#    def forward(self,x):
#        out = self.layer1(x)
#        out = self.layer2(out)
#        out = self.layer3(out)
#        out = self.layer4(out)
#        out = self.layer5(out)
#        return out
       