
import os
import warnings
warnings.filterwarnings("ignore")

from testmodel_choose import TestModel
#from testattack import AttackModel
from generate_attack_image import AttackModel1
from attackmodel_choose import AttackModel2
from attackmodel_choose2 import AttackModel3
ac_gate = 0
while(ac_gate == 0):
    print("\nThe mode has the following...")
    print("(1) Mode 1: test")
    print("(2) Mode 2: attack")
    action = input("Please choose a mode(1~2): ")
    os.system('cls')
    if action == '1':
        TestModel(action)
        ac_gate=1
    elif action == '2':
        print("\nThe attack mode has the following...")
        print("(1) Mode 1: generate attack images from condition-dcgan")
        print("(2) Mode 2: input our attack images and show the attack success rate")
        print("(3) Mode 3: iuput your dataset and show the attack success rate")
        action1 = input("Please choose a attack mode(1~3): \n")
        os.system('cls') 
        if action1 == '1':
            AttackModel1()
            ac_gate=1
        elif action1 == '2':
            AttackModel2(action1)
            ac_gate=1
        elif action1 == '3':
            AttackModel3(action1)
            ac_gate=1
