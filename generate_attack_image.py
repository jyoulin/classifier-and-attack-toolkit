import argparse
import torch
torch.cuda.empty_cache()
import torchvision
import torchvision.utils as vutils
#from torchvision import transforms
import torch.nn as nn
from random import randint
from cdcgan_genarator.model import NetD, NetG
import os
import sys
from torchvision import datasets
from torch.utils.data import Dataset, DataLoader
from torch.autograd import Variable
from torchvision.utils import save_image
#os.environ['CUDA_VISIBLE_DEVICES']='1'
import torchvision.models as models
import gc
#del variable #delete unnecessary variables 
gc.collect()
#model = nn.DataParallel(models.resnet18())
import numpy as np
import matplotlib.image as mpimg

class AttackModel1():
    def __init__(self):
        self.generate()

    def generate(self):

      parser = argparse.ArgumentParser()
      parser.add_argument('--batchSize', type=int, default=16)
      parser.add_argument('--imageSize', type=int, default=96)
      parser.add_argument('--nz', type=int, default=100, help='size of the latent z vector')
#plus
      parser.add_argument("--n_classes", type=int, default=135, help="number of classes for dataset")
      parser.add_argument('--ngf', type=int, default=64)
      parser.add_argument('--ndf', type=int, default=64)
      parser.add_argument('--epoch', type=int, default=1000, help='number of epochs to train for')
      parser.add_argument('--lr', type=float, default=0.0002, help='learning rate, default=0.0002')
      parser.add_argument('--beta1', type=float, default=0.5, help='beta1 for adam. default=0.5')
      parser.add_argument('--data_path', default='train_15', help='folder to train data')
      parser.add_argument('--outf', default='cdcgan_15/', help='folder to output images and model checkpoints')
      parser.add_argument('--sample_interval', type=int, default=120, help='interval between image sampling')
      opt = parser.parse_args()
      device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

#if choose attack
      name = input('please choose your input dataset：\n'
       '1. generate attack images from conditional-dcgan\n'
       '2. generate single attack image from conditional-dcgan\n' 
       )
      name1 = int(name)

      if name1 == 1 :
        model_name = input('please choose the model which you want to attack: \n'
        '1. actual thermal image - 15 classes\n'
        '2. actual thermal image - 15+1(other picture) classes\n'
        '3. actual thermal image - 15+1(gan picture) classes\n'
        '4. actual thermal image - 30 classes\n'
        '5. actual thermal image - 30+1(other picture) classes\n'
        '6. actual thermal image - 30+1(gan picture) classes\n'
        '7. actual thermal image - 60 classes\n'
        '8. actual thermal image - 60+1(other picture) classes\n'
        '9. actual thermal image - 60+1(gan picture) classes\n'
        '10. simulate thermal image - 84+1 classes\n'   
        '11. simulate thermal image - 84+1(other picture) classes\n'  
        '12. simulate thermal image - 84+1(gan picture) classes\n'
        '13. simulate thermal image - 84dcgan+1 classes\n'   
        '14. simulate thermal image - 84dcgan+1(other picture) classes\n'  
        '15. simulate thermal image - 84dcgan+1(gan picture) classes\n'
        '16. simulate thermal image - 135+1 classes\n'   
        '17. simulate thermal image - 135+1(other picture) classes\n'  
        '18. simulate thermal image - 135+1(gan picture) classes\n'    
        )
        model_name = int(model_name)
        if model_name == 1 or model_name == 2 or model_name == 3:
          opt.data_path = 'train_15'
          opt.n_classes = 15
          from cdcgan_genarator.model15 import NetD, NetG
        elif model_name == 4 or model_name == 5 or model_name == 6:
          opt.data_path = 'train_30'
          opt.n_classes = 30
          from cdcgan_genarator.model30 import NetD, NetG
        elif model_name == 7 or model_name == 8 or model_name == 9:
          opt.data_path = 'train_60'
          opt.n_classes = 60
          from cdcgan_genarator.model60 import NetD, NetG
        elif model_name == 10 or model_name == 11 or model_name == 12:
          opt.data_path = 'train_84'
          opt.n_classes = 84
          from cdcgan_genarator.model84 import NetD, NetG
        elif model_name == 13 or model_name == 14 or model_name == 15:
          opt.data_path = 'train_84dcgan'
          opt.n_classes = 84
          from cdcgan_genarator.model84 import NetD, NetG
        elif model_name == 16 or model_name == 17 or model_name == 18:
          opt.data_path = 'train_135'
          opt.n_classes = 135
          from cdcgan_genarator.model135 import NetD, NetG
        else:
          print("wrong model number !!! please choose the model again")
          sys.exit()
        number = input('please input the number of pictures per class which you want to generate (MAX:100)：')
        number = int(number)
        if number > 100 :
          print("max number is 100!")
          sys.exit()
  #path
        src_path = './generate_image_directory/generate_image_1pic/' + opt.data_path 
  #print(src_path)
  #print(opt.n_classes) 
        output_path = "./generate_image/" + opt.data_path         
        path_model = './model/generator_pth/'+ opt.data_path+ '/'
        path_state_dict = './model/generator_pth/'+ opt.data_path+ '/'
        dirs = [d for d in os.listdir(src_path) if os.path.isdir(os.path.join(src_path,d))]
        dirs.sort()
        dirs_class = [d for d in os.listdir(src_path) if os.path.isdir(os.path.join(src_path,d))]
        dirs_class.sort()
        loadpth_path = './model/generator_pth/'+opt.data_path
        loadpth = [d for d in os.listdir(loadpth_path) if os.path.isfile(os.path.join(loadpth_path,d))]
        loadpth.sort()
  #print(loadpth)
        loadpth.reverse()
  #create new folder
        if not output_path:
          os.makedirs(output_path,exist_ok=True)
        for i in dirs:
         folder = os.path.exists(output_path+i)
         if not folder:
           os.makedirs(output_path+'/'+i,exist_ok=True)
     #print(folder)
  #model parameter
        netG = NetG(opt.ngf, opt.nz).to(device)
        netD = NetD(opt.ndf).to(device)

        criterion = nn.BCELoss()
        optimizerG = torch.optim.Adam(netG.parameters(), lr=opt.lr, betas=(opt.beta1, 0.999))
        optimizerD = torch.optim.Adam(netD.parameters(), lr=opt.lr, betas=(opt.beta1, 0.999))

        label = torch.FloatTensor(opt.batchSize)
  #print(label.shape)
        real_label = 1
        fake_label = 0

        Batch_Size = opt.batchSize
        N_Class = opt.n_classes
        img_size = opt.imageSize
  # Adversarial ground truths
        valid = Variable(torch.ones(Batch_Size).cuda(), requires_grad=False)
        fake = Variable(torch.zeros(Batch_Size).cuda(), requires_grad=False)
        print("loading model...")
        for i in range(1,number+1):
    #print(i)
          pth = loadpth[-1+i] 
          #print(pth)
          all_pth = path_model+pth 
          #print(all_pth)
          G = torch.load(all_pth)
          G.train()
    #print("generating images...")
          optimizerG.zero_grad()
          noise = Variable(torch.FloatTensor(np.random.normal(0, 1, (N_Class, opt.nz,1,1))).cuda())
          optimizerG.zero_grad()    
          y_ = torch.LongTensor(np.array([num for num in range(N_Class)])).view(1,N_Class)
          optimizerG.zero_grad()
          y_fixed = torch.zeros(N_Class, N_Class)
          y_fixed = Variable(y_fixed.scatter_(1,y_.view(N_Class,1),1).view(N_Class, N_Class,1,1).cuda())
          optimizerG.zero_grad()
          gen_imgs = G(noise, y_fixed).view(-1,3,opt.imageSize,opt.imageSize)
        #save_image(gen_imgs.data,'666.png') 
          optimizerG.zero_grad()
     
          classes = 0
          #plus
          if not 'generate_image/' + opt.data_path:
              os.makedirs('generate_image/' + opt.data_path,exist_ok=True)
              
          for j in dirs_class:         
          #print('attack_image/' + name + '/'+j+'/'+str(i)+'.png')              
            save_image(gen_imgs.data[0+classes], 'generate_image/' + opt.data_path + '/'+j+'/no.'+str(i)+'.png', nrow=15, normalize=True)    
            classes = classes+1 
            print("generate class" + str(classes) + " no."+str(i)+"... picture")
        print("finished!") 
        print("generate images is in the directory of "+'./generate_image/' + opt.data_path +'/')
      #success rate
        #print("success rate:")
      elif name1 == 2:
        model_name = input('please choose the model which you want to attack: \n'
      '1. actual thermal image - 15 classes\n'
      '2. actual thermal image - 15+1(other picture) classes\n'
      '3. actual thermal image - 15+1(gan picture) classes\n'
      '4. actual thermal image - 30 classes\n'
      '5. actual thermal image - 30+1(other picture) classes\n'
      '6. actual thermal image - 30+1(gan picture) classes\n'
      '7. actual thermal image - 60 classes\n'
      '8. actual thermal image - 60+1(other picture) classes\n'
      '9. actual thermal image - 60+1(gan picture) classes\n'
      '10. simulate thermal image - 84+1 classes\n'   
      '11. simulate thermal image - 84+1(other picture) classes\n'  
      '12. simulate thermal image - 84+1(gan picture) classes\n'
      '13. simulate thermal image - 84dcgan+1 classes\n'   
      '14. simulate thermal image - 84dcgan+1(other picture) classes\n'  
      '15. simulate thermal image - 84dcgan+1(gan picture) classes\n'
      '16. simulate thermal image - 135+1 classes\n'   
      '17. simulate thermal image - 135+1(other picture) classes\n'  
      '18. simulate thermal image - 135+1(gan picture) classes\n'    
        )
        model_name = int(model_name)
        if model_name == 1 or model_name == 2 or model_name == 3:
          opt.data_path = 'train_15'
          opt.n_classes = 15
          from cdcgan_genarator.model15 import NetD, NetG
        elif model_name == 4 or model_name == 5 or model_name == 6:
          opt.data_path = 'train_30'
          opt.n_classes = 30
          from cdcgan_genarator.model30 import NetD, NetG
        elif model_name == 7 or model_name == 8 or model_name == 9:
          opt.data_path = 'train_60'
          opt.n_classes = 60
          from cdcgan_genarator.model60 import NetD, NetG
        elif model_name == 10 or model_name == 11 or model_name == 12:
          opt.data_path = 'train_84'
          opt.n_classes = 84
          from cdcgan_genarator.model84 import NetD, NetG
        elif model_name == 13 or model_name == 14 or model_name == 15:
          opt.data_path = 'train_84dcgan'
          opt.n_classes = 84
          from cdcgan_genarator.model84 import NetD, NetG
        elif model_name == 16 or model_name == 17 or model_name == 18:
          opt.data_path = 'train_135'
          opt.n_classes = 135
          from cdcgan_genarator.model135 import NetD, NetG
        else:
          print("wrong model number !!! please choose the model again")
          sys.exit()
        
        number = 1
      #path
        src_path = './generate_image_directory/generate_image_1pic/' + opt.data_path 
      #print(src_path)
      #print(opt.n_classes) 
        output_path = "./generate_image_1pic/" + opt.data_path         
        path_model = './model/generator_pth/'+ opt.data_path+ '/'
        path_state_dict = './model/generator/'+ opt.data_path+ '/'
        dirs = [d for d in os.listdir(src_path) if os.path.isdir(os.path.join(src_path,d))]
        dirs.sort()
        dirs_class = [d for d in os.listdir(src_path) if os.path.isdir(os.path.join(src_path,d))]
        dirs_class.sort()
        loadpth_path = './model/generator_pth/'+opt.data_path
        loadpth = [d for d in os.listdir(loadpth_path) if os.path.isfile(os.path.join(loadpth_path,d))]
        loadpth.sort()
      #print(loadpth)
        loadpth.reverse()
      #create new folder
        if not output_path:
          os.makedirs(output_path,exist_ok=True)
        for i in dirs:
         folder = os.path.exists(output_path+i)
         if not folder:
           os.makedirs(output_path+'/'+i,exist_ok=True)
         #print(folder)
      #model parameter
        netG = NetG(opt.ngf, opt.nz).to(device)
        netD = NetD(opt.ndf).to(device)
    
        criterion = nn.BCELoss()
        optimizerG = torch.optim.Adam(netG.parameters(), lr=opt.lr, betas=(opt.beta1, 0.999))
        optimizerD = torch.optim.Adam(netD.parameters(), lr=opt.lr, betas=(opt.beta1, 0.999))
    
        label = torch.FloatTensor(opt.batchSize)
      #print(label.shape)
        real_label = 1
        fake_label = 0
      
        Batch_Size = opt.batchSize
        N_Class = opt.n_classes
        img_size = opt.imageSize
      # Adversarial ground truths
        valid = Variable(torch.ones(Batch_Size).cuda(), requires_grad=False)
        fake = Variable(torch.zeros(Batch_Size).cuda(), requires_grad=False)
        print("loading model...")
        for i in range(1,number+1):
        #print(i)
          pth = loadpth[-1+i] 
        #print(pth)
          all_pth = path_model+pth  
          G = torch.load(all_pth)
          G.train()
        #print("generating images...")
          optimizerG.zero_grad()
          noise = Variable(torch.FloatTensor(np.random.normal(0, 1, (N_Class, opt.nz,1,1))).cuda())
          optimizerG.zero_grad()    
          y_ = torch.LongTensor(np.array([num for num in range(N_Class)])).view(1,N_Class)
          optimizerG.zero_grad()
          y_fixed = torch.zeros(N_Class, N_Class)
          y_fixed = Variable(y_fixed.scatter_(1,y_.view(N_Class,1),1).view(N_Class, N_Class,1,1).cuda())
          optimizerG.zero_grad()
          gen_imgs = G(noise, y_fixed).view(-1,3,opt.imageSize,opt.imageSize)
        #save_image(gen_imgs.data,'666.png') 
          optimizerG.zero_grad()
     
          classes = 0
          for j in dirs_class:
          
          #print('attack_image/' + name + '/'+j+'/'+str(i)+'.png')
            save_image(gen_imgs.data[0+classes], 'generate_image_1pic/' + opt.data_path + '/'+j+'/no.'+str(i)+'.png', nrow=15, normalize=True)    
            classes = classes+1 
            print("generate class" + str(classes) + " no."+str(i)+"... picture")
        print("finished!")
        print("generate images is in the directory of "+'./generate_image_1pic/' + opt.data_path +'/')
      #run result program
        #print("result:") 
      elif name1 == 3:
        print('Warning!!! please put your images to the specify folder')
      # run accuracy program
        number = input('please input the number of pictures per class which you want to generate (MAX:100)：')
        number = int(number)
        print('success rate...')
      elif name1 == 4:
        print('Warning!!! please put your single image to the specify folder')
      #  run classify program
        print('show classify result...')
      else :
        print("wrong choose !!! please choose again")
