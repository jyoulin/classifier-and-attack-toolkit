import os
import numpy as np
import pandas as pd
import cv2
from keras.utils import np_utils
pd.set_option('display.max_rows',10000)

 


class LoadAttackData2():
    
    def load_15_attack_data(self):
        imsize = 224
        attack_path = "./dataset/input_attack_data/15classes/"
        attack_folderpath = []
        attack_filefullpath = []
        attack_datalabel = []
        attack_data = []
        attack_y = []
        
        for attack_folder in os.listdir(attack_path):
            attack_folderpath.append(os.path.join(attack_path, attack_folder))
        for attack_catchfolderpath in attack_folderpath:
            for attack_file in os.listdir(attack_catchfolderpath):
                attack_filefullpath.append(os.path.join(attack_catchfolderpath, attack_file))
                attack_datalabel.append(attack_catchfolderpath.split('/')[4])
        
        attack_data_dict = {'attack_image_path': attack_filefullpath, 'attack_label': attack_datalabel}
        attack_my_data = pd.DataFrame(attack_data_dict)
        
        attack_classlabel = {'500rpm_16to345': 0,
                             '500rpm_376to705': 1,
                             '500rpm_736to1065': 2,
                             '500rpm_1096to1425': 3,
                             '500rpm_1456to1785': 4,
                             '1000rpm_16to225': 5,
                             '1000rpm_256to465': 6,
                             '1000rpm_496to705': 7,
                             '1000rpm_736to945': 8,
                             '1000rpm_976to1185': 9,
                             '1500rpm_16to165': 10,
                             '1500rpm_196to345': 11,
                             '1500rpm_376to525': 12,
                             '1500rpm_556to705': 13,
                             '1500rpm_736to885': 14,
                             }
        
        attack_my_data['attack_label'] = attack_my_data['attack_label'].map(attack_classlabel)
        attack_my_data = attack_my_data.sort_values('attack_label')
        #print(attack_my_data)
        #attack_my_data.to_csv('test.csv')
        attack_my_data = attack_my_data.reset_index(drop=True)
        att_y = attack_my_data['attack_label']
        a = list(attack_my_data['attack_image_path']) 
        #print(att_y)
        attack_img_size = (imsize, imsize)
        
        for attack_i in range(len(a)):
            attack_img_bgr = cv2.imread(a[attack_i])
            attack_img_rgb = attack_img_bgr[:, :, ::-1]
            attack_img_fixsize = cv2.resize(attack_img_rgb, attack_img_size)
            attack_data.append(attack_img_fixsize)
            attack_y.append(att_y[attack_i])
        
        attack_data_array = np.array(attack_data)
        x_attack = attack_data_array
        y_attack = np_utils.to_categorical(attack_y)
        # y_attack = attack_y
        x_attack = x_attack.astype('float32') / 255.0
        #print(x_attack)
        #print(y_attack)
        return(x_attack, y_attack)
    
    def load_30_attack_data(self):
        imsize = 224
        attack_path = "./dataset/input_attack_data/30classes/"
        attack_folderpath = []
        attack_filefullpath = []
        attack_datalabel = []
        attack_data = []
        attack_y = []
        
        for attack_folder in os.listdir(attack_path):
            attack_folderpath.append(os.path.join(attack_path, attack_folder))
        for attack_catchfolderpath in attack_folderpath:
            for attack_file in os.listdir(attack_catchfolderpath):
                attack_filefullpath.append(os.path.join(attack_catchfolderpath, attack_file))
                attack_datalabel.append(attack_catchfolderpath.split('/')[4])
        
        attack_data_dict = {'attack_image_path': attack_filefullpath, 'attack_label': attack_datalabel}
        attack_my_data = pd.DataFrame(attack_data_dict)
        
        attack_classlabel = {'500rpm_0to137s': 0,
                             '500rpm_138to275s': 1,
                             '500rpm_276to413s': 2,
                             '500rpm_414to551s': 3,
                             '500rpm_552to689s': 4,
                             '500rpm_690to827s': 5,
                             '500rpm_828to965s': 6,
                             '500rpm_966to1103s': 7,
                             '500rpm_1104to1241s': 8,
                             '500rpm_1242to1379s': 9,
                             '500rpm_1380to1517s': 10,
                             '500rpm_1518to1655s': 11,
                             '500rpm_1656to1793s': 12,
                             '1000rpm_0to132s': 13,
                             '1000rpm_133to265s': 14,
                             '1000rpm_266to398s': 15,
                             '1000rpm_399to531s': 16,
                             '1000rpm_532to664s': 17,
                             '1000rpm_665to797s': 18,
                             '1000rpm_798to930s': 19,
                             '1000rpm_931to1063s': 20,
                             '1000rpm_1064to1196s': 21,
                             '1500rpm_0to111s': 22,
                             '1500rpm_112to223s': 23,
                             '1500rpm_224to335s': 24,
                             '1500rpm_336to447s': 25,
                             '1500rpm_448to559s': 26,
                             '1500rpm_560to671s': 27,
                             '1500rpm_672to783s': 28,
                             '1500rpm_784to895s': 29}
        
        attack_my_data['attack_label'] = attack_my_data['attack_label'].map(attack_classlabel)
        attack_my_data = attack_my_data.sort_values('attack_label')
        attack_my_data = attack_my_data.reset_index(drop=True)
        att_y = attack_my_data['attack_label']
        
        a = list(attack_my_data['attack_image_path']) 
        #print(att_y)
        attack_img_size = (imsize, imsize)
        
        for attack_i in range(len(a)):
            attack_img_bgr = cv2.imread(a[attack_i])
            attack_img_rgb = attack_img_bgr[:, :, ::-1]
            attack_img_fixsize = cv2.resize(attack_img_rgb, attack_img_size)
            attack_data.append(attack_img_fixsize)
            attack_y.append(att_y[attack_i])
        
        attack_data_array = np.array(attack_data)
        x_attack = attack_data_array
        y_attack = np_utils.to_categorical(attack_y)
        # y_attack = attack_y
        x_attack = x_attack.astype('float32') / 255.0
        return(x_attack, y_attack)
    
    def load_60_attack_data(self):
        imsize = 224
        attack_path = "./dataset/input_attack_data/60classes/"
        attack_folderpath = []
        attack_filefullpath = []
        attack_datalabel = []
        attack_data = []
        attack_y = []
        
        for attack_folder in os.listdir(attack_path):
            attack_folderpath.append(os.path.join(attack_path, attack_folder))
        for attack_catchfolderpath in attack_folderpath:
            for attack_file in os.listdir(attack_catchfolderpath):
                attack_filefullpath.append(os.path.join(attack_catchfolderpath, attack_file))
                attack_datalabel.append(attack_catchfolderpath.split('/')[4])
        
        attack_data_dict = {'attack_image_path': attack_filefullpath, 'attack_label': attack_datalabel}
        attack_my_data = pd.DataFrame(attack_data_dict)
        
        attack_classlabel = {'500rpm_0to68s': 0,
                  '500rpm_69to137s': 1,
                  '500rpm_138to206s': 2,
                  '500rpm_207to275s': 3,
                  '500rpm_276to344s': 4,
                  '500rpm_345to413s': 5,
                  '500rpm_414to482s': 6,
                  '500rpm_483to551s': 7,
                  '500rpm_552to620s': 8,
                  '500rpm_621to689s': 9,
                  '500rpm_690to758s': 10,
                  '500rpm_759to827s': 11,
                  '500rpm_828to896s': 12,
                  '500rpm_897to965s': 13,
                  '500rpm_966to1034s': 14,
                  '500rpm_1035to1103s': 15,
                  '500rpm_1104to1172s': 16,
                  '500rpm_1173to1241s': 17,
                  '500rpm_1242to1310s': 18,
                  '500rpm_1311to1379s': 19,
                  '500rpm_1380to1448s': 20,
                  '500rpm_1449to1517s': 21,
                  '500rpm_1518to1586s': 22,
                  '500rpm_1587to1655s': 23,
                  '500rpm_1656to1724s': 24,
                  '500rpm_1725to1793s': 25,
                  '1000rpm_0to62s': 26,
                  '1000rpm_63to125s': 27,
                  '1000rpm_126to188s': 28,
                  '1000rpm_189to251s': 29,
                  '1000rpm_252to314s': 30,
                  '1000rpm_315to377s': 31,
                  '1000rpm_378to440s': 32,
                  '1000rpm_441to503s': 33,
                  '1000rpm_504to566s': 34,
                  '1000rpm_567to629s': 35,
                  '1000rpm_630to692s': 36,
                  '1000rpm_693to755s': 37,
                  '1000rpm_756to818s': 38,
                  '1000rpm_819to881s': 39,
                  '1000rpm_882to944s': 40,
                  '1000rpm_945to1007s': 41,
                  '1000rpm_1008to1070s': 42,
                  '1000rpm_1071to1133s': 43,
                  '1000rpm_1134to1196s': 44,
                  '1500rpm_0to59s': 45,
                  '1500rpm_60to119s': 46,
                  '1500rpm_120to179s': 47,
                  '1500rpm_180to239s': 48,
                  '1500rpm_240to299s': 49,
                  '1500rpm_300to359s': 50,
                  '1500rpm_360to419s': 51,
                  '1500rpm_420to479s': 52,
                  '1500rpm_480to539s': 53,
                  '1500rpm_540to599s': 54,
                  '1500rpm_600to659s': 55,
                  '1500rpm_660to719s': 56,
                  '1500rpm_720to779s': 57,
                  '1500rpm_780to839s': 58,
                  '1500rpm_840to899s': 59}
        
        attack_my_data['attack_label'] = attack_my_data['attack_label'].map(attack_classlabel)
        attack_my_data = attack_my_data.sort_values('attack_label')
        attack_my_data = attack_my_data.reset_index(drop=True)
        att_y = attack_my_data['attack_label']
        
        a = list(attack_my_data['attack_image_path']) 
        #print(att_y)
        attack_img_size = (imsize, imsize)
        
        for attack_i in range(len(a)):
            attack_img_bgr = cv2.imread(a[attack_i])
            attack_img_rgb = attack_img_bgr[:, :, ::-1]
            attack_img_fixsize = cv2.resize(attack_img_rgb, attack_img_size)
            attack_data.append(attack_img_fixsize)
            attack_y.append(att_y[attack_i])
        
        attack_data_array = np.array(attack_data)
        x_attack = attack_data_array
        y_attack = np_utils.to_categorical(attack_y)
        # y_attack = attack_y
        x_attack = x_attack.astype('float32') / 255.0
        return(x_attack, y_attack)
    
    def load_135_attack_data(self, model):
        if(model == '1'):
            imsize = 28
        elif(model == '2'):
            imsize = 64
        elif(model == '3'):
            imsize = 75
        elif(model == '4'):
            imsize = 32
        attack_path = "./dataset/input_attack_data/135classes/"
        attack_folderpath = []
        attack_filefullpath = []
        attack_datalabel = []
        attack_data = []
        attack_y = []
        
        for attack_folder in os.listdir(attack_path):
            attack_folderpath.append(os.path.join(attack_path, attack_folder))
        for attack_catchfolderpath in attack_folderpath:
            for attack_file in os.listdir(attack_catchfolderpath):
                attack_filefullpath.append(os.path.join(attack_catchfolderpath, attack_file))
                attack_datalabel.append(attack_catchfolderpath.split('/')[4])
        
        attack_data_dict = {'attack_image_path': attack_filefullpath, 'attack_label': attack_datalabel}
        attack_my_data = pd.DataFrame(attack_data_dict)
        
        attack_classlabel = {'100rpm_90to1108': 0,
                  '100rpm_1198to2216': 1,
                  '100rpm_2306to3324': 2,
                  '100rpm_3414to4432': 3,
                  '100rpm_4522to5540': 4,
                  '100rpm_5630to6648': 5,
                  '100rpm_6738to7756': 6,
                  '100rpm_7846to8864': 7,
                  '100rpm_8954to9972': 8,
                  '200rpm_90to1108': 9,
                  '200rpm_1198to2216': 10,
                  '200rpm_2306to3324': 11,
                  '200rpm_3414to4432': 12,
                  '200rpm_4522to5540': 13,
                  '200rpm_5630to6648': 14,
                  '200rpm_6738to7756': 15,
                  '200rpm_7846to8864': 16,
                  '200rpm_8954to9972': 17,
                  '300rpm_90to1108': 18,
                  '300rpm_1198to2216': 19,
                  '300rpm_2306to3324': 20,
                  '300rpm_3414to4432': 21,
                  '300rpm_4522to5540': 22,
                  '300rpm_5630to6648': 23,
                  '300rpm_6738to7756': 24,
                  '300rpm_7846to8864': 25,
                  '300rpm_8954to9972': 26,
                  '400rpm_90to1108': 27,
                  '400rpm_1198to2216': 28,
                  '400rpm_2306to3324': 29,
                  '400rpm_3414to4432': 30,
                  '400rpm_4522to5540': 31,
                  '400rpm_5630to6648': 32,
                  '400rpm_6738to7756': 33,
                  '400rpm_7846to8864': 34,
                  '400rpm_8954to9972': 35,
                  '500rpm_90to1108': 36,
                  '500rpm_1198to2216': 37,
                  '500rpm_2306to3324': 38,
                  '500rpm_3414to4432': 39,
                  '500rpm_4522to5540': 40,
                  '500rpm_5630to6648': 41,
                  '500rpm_6738to7756': 42,
                  '500rpm_7846to8864': 43,
                  '500rpm_8954to9972': 44,
                  '600rpm_90to1108': 45,
                  '600rpm_1198to2216': 46,
                  '600rpm_2306to3324': 47,
                  '600rpm_3414to4432': 48,
                  '600rpm_4522to5540': 49,
                  '600rpm_5630to6648': 50,
                  '600rpm_6738to7756': 51,
                  '600rpm_7846to8864': 52,
                  '600rpm_8954to9972': 53,
                  '700rpm_90to1108': 54,
                  '700rpm_1198to2216': 55,
                  '700rpm_2306to3324': 56,
                  '700rpm_3414to4432': 57,
                  '700rpm_4522to5540': 58,
                  '700rpm_5630to6648': 59,
                  '700rpm_6738to7756': 60,
                  '700rpm_7846to8864': 61,
                  '700rpm_8954to9972': 62,
                  '800rpm_90to1108': 63,
                  '800rpm_1198to2216': 64,
                  '800rpm_2306to3324': 65,
                  '800rpm_3414to4432': 66,
                  '800rpm_4522to5540': 67,
                  '800rpm_5630to6648': 68,
                  '800rpm_6738to7756': 69,
                  '800rpm_7846to8864': 70,
                  '800rpm_8954to9972': 71,
                  '900rpm_90to1108': 72,
                  '900rpm_1198to2216': 73,
                  '900rpm_2306to3324': 74,
                  '900rpm_3414to4432': 75,
                  '900rpm_4522to5540': 76,
                  '900rpm_5630to6648': 77,
                  '900rpm_6738to7756': 78,
                  '900rpm_7846to8864': 79,
                  '900rpm_8954to9972': 80,
                  '1000rpm_90to1108': 81,
                  '1000rpm_1198to2216': 82,
                  '1000rpm_2306to3324': 83,
                  '1000rpm_3414to4432': 84,
                  '1000rpm_4522to5540': 85,
                  '1000rpm_5630to6648': 86,
                  '1000rpm_6738to7756': 87,
                  '1000rpm_7846to8864': 88,
                  '1000rpm_8954to9972': 89,
                  '1100rpm_90to1108': 90,
                  '1100rpm_1198to2216': 91,
                  '1100rpm_2306to3324': 92,
                  '1100rpm_3414to4432': 93,
                  '1100rpm_4522to5540': 94,
                  '1100rpm_5630to6648': 95,
                  '1100rpm_6738to7756': 96,
                  '1100rpm_7846to8864': 97,
                  '1100rpm_8954to9972': 98,
                  '1200rpm_90to1108': 99,
                  '1200rpm_1198to2216': 100,
                  '1200rpm_2306to3324': 101,
                  '1200rpm_3414to4432': 102,
                  '1200rpm_4522to5540': 103,
                  '1200rpm_5630to6648': 104,
                  '1200rpm_6738to7756': 105,
                  '1200rpm_7846to8864': 106,
                  '1200rpm_8954to9972': 107,
                  '1300rpm_90to1108': 108,
                  '1300rpm_1198to2216': 109,
                  '1300rpm_2306to3324': 110,
                  '1300rpm_3414to4432': 111,
                  '1300rpm_4522to5540': 112,
                  '1300rpm_5630to6648': 113,
                  '1300rpm_6738to7756': 114,
                  '1300rpm_7846to8864': 115,
                  '1300rpm_8954to9972': 116,
                  '1400rpm_90to1108': 117,
                  '1400rpm_1198to2216': 118,
                  '1400rpm_2306to3324': 119,
                  '1400rpm_3414to4432': 120,
                  '1400rpm_4522to5540': 121,
                  '1400rpm_5630to6648': 122,
                  '1400rpm_6738to7756': 123,
                  '1400rpm_7846to8864': 124,
                  '1400rpm_8954to9972': 125,
                  '1500rpm_90to1108': 126,
                  '1500rpm_1198to2216': 127,
                  '1500rpm_2306to3324': 128,
                  '1500rpm_3414to4432': 129,
                  '1500rpm_4522to5540': 130,
                  '1500rpm_5630to6648': 131,
                  '1500rpm_6738to7756': 132,
                  '1500rpm_7846to8864': 133,
                  '1500rpm_8954to9972': 134}
        
        attack_my_data['attack_label'] = attack_my_data['attack_label'].map(attack_classlabel)
        attack_my_data = attack_my_data.sort_values('attack_label')
        attack_my_data = attack_my_data.reset_index(drop=True)
        att_y = attack_my_data['attack_label']
        
        a = list(attack_my_data['attack_image_path']) 
        #print(att_y)
        attack_img_size = (imsize, imsize)
        
        for attack_i in range(len(a)):
            attack_img_bgr = cv2.imread(a[attack_i])
            attack_img_rgb = attack_img_bgr[:, :, ::-1]
            attack_img_fixsize = cv2.resize(attack_img_rgb, attack_img_size)
            attack_data.append(attack_img_fixsize)
            attack_y.append(att_y[attack_i])
        
        attack_data_array = np.array(attack_data)
        x_attack = attack_data_array
        y_attack = np_utils.to_categorical(attack_y)
        # y_attack = attack_y
        x_attack = x_attack.astype('float32') / 255.0
        return(x_attack, y_attack)
    
    def load_84_attack_data(self, model):
        if(model == '1'):
            imsize = 28
        elif(model == '2'):
            imsize = 32
        elif(model == '3'):
            imsize = 75
        elif(model == '4'):
            imsize = 32
        attack_path = "./dataset/input_attack_data/84classes_dcgan/"
        attack_folderpath = []
        attack_filefullpath = []
        attack_datalabel = []
        attack_data = []
        attack_y = []
        
        for attack_folder in os.listdir(attack_path):
            attack_folderpath.append(os.path.join(attack_path, attack_folder))
        for attack_catchfolderpath in attack_folderpath:
            for attack_file in os.listdir(attack_catchfolderpath):
                attack_filefullpath.append(os.path.join(attack_catchfolderpath, attack_file))
                attack_datalabel.append(attack_catchfolderpath.split('/')[4])
        
        attack_data_dict = {'attack_image_path': attack_filefullpath, 'attack_label': attack_datalabel}
        attack_my_data = pd.DataFrame(attack_data_dict)
        
        attack_classlabel = {'100rpm_1to475': 0,
                  '100rpm_476to1190': 1,
                  '100rpm_1191to3250': 2,
                  '100rpm_3251to5940': 3,
                  '100rpm_5941to8790': 4,
                  '100rpm_8791to10799': 5,
                  '200rpm_1to690': 6,
                  '200rpm_691to1640': 7,
                  '200rpm_1641to3220': 8,
                  '200rpm_3221to6185': 9,
                  '200rpm_6186to7915': 10,
                  '200rpm_7916to10799': 11,
                  '300rpm_1to775': 12,
                  '300rpm_776to1700': 13,
                  '300rpm_1701to3590': 14,
                  '300rpm_3591to6525': 15,
                  '300rpm_6526to8545': 16,
                  '300rpm_8546to10799': 17,
                  '400rpm_1to640': 18,
                  '400rpm_641to1385': 19,
                  '400rpm_1386to1900': 20,
                  '400rpm_1901to3960': 21,
                  '400rpm_3961to7100': 22,
                  '400rpm_7101to10799': 23,
                  '500rpm_1to690': 24,
                  '500rpm_691to1380': 25,
                  '500rpm_1381to2205': 26,
                  '500rpm_2206to3700': 27,
                  '500rpm_3701to7055': 28,
                  '500rpm_7056to10799': 29,
                  '600rpm_1to505': 30,
                  '600rpm_506to1070': 31,
                  '600rpm_1071to2135': 32,
                  '600rpm_2136to4625': 33,
                  '600rpm_4626to6325': 34,
                  '600rpm_6326to10799': 35,
                  '700rpm_1to750': 36,
                  '700rpm_751to1810': 37,
                  '700rpm_1811to3065': 38,
                  '700rpm_3066to6655': 39,
                  '700rpm_6656to8080': 40,
                  '700rpm_8081to10799': 41,
                  '800rpm_1to575': 42,
                  '800rpm_576to2110': 43,
                  '800rpm_2111to4560': 44,
                  '800rpm_4561to7655': 45,
                  '800rpm_7656to10799': 46,
                  '900rpm_1to580': 47,
                  '900rpm_581to1655': 48,
                  '900rpm_1656to3480': 49,
                  '900rpm_3481to6660': 50,
                  '900rpm_6661to8250': 51,
                  '900rpm_8251to10799': 52,
                  '1000rpm_1to800': 53,
                  '1000rpm_801to1720': 54,
                  '1000rpm_1721to3310': 55,
                  '1000rpm_3311to5175': 56,
                  '1000rpm_5176to8500': 57,
                  '1000rpm_8501to10799': 58,
                  '1100rpm_1to800': 59,
                  '1100rpm_801to2950': 60,
                  '1100rpm_2951to5510': 61,
                  '1100rpm_5511to8000': 62,
                  '1100rpm_8001to10799': 63,
                  '1200rpm_1to700': 64,
                  '1200rpm_701to2780': 65,
                  '1200rpm_2781to5910': 66,
                  '1200rpm_5911to7780': 67,
                  '1200rpm_7781to10799': 68,
                  '1300rpm_1to590': 69,
                  '1300rpm_591to2400': 70,
                  '1300rpm_2401to5710': 71,
                  '1300rpm_5711to8120': 72,
                  '1300rpm_8121to10799': 73,
                  '1400rpm_1to580': 74,
                  '1400rpm_581to2190': 75,
                  '1400rpm_2191to5900': 76,
                  '1400rpm_5901to8730': 77,
                  '1400rpm_8731to10799': 78,
                  '1500rpm_1to450': 79,
                  '1500rpm_451to1900': 80,
                  '1500rpm_1901to5200': 81,
                  '1500rpm_5201to8600': 82,
                  '1500rpm_8601to10799': 83}
        
        attack_my_data['attack_label'] = attack_my_data['attack_label'].map(attack_classlabel)
        attack_my_data = attack_my_data.sort_values('attack_label')
        attack_my_data = attack_my_data.reset_index(drop=True)
        att_y = attack_my_data['attack_label']
        
        a = list(attack_my_data['attack_image_path']) 
        #print(att_y)
        attack_img_size = (imsize, imsize)
        
        for attack_i in range(len(a)):
            attack_img_bgr = cv2.imread(a[attack_i])
            attack_img_rgb = attack_img_bgr[:, :, ::-1]
            attack_img_fixsize = cv2.resize(attack_img_rgb, attack_img_size)
            attack_data.append(attack_img_fixsize)
            attack_y.append(att_y[attack_i])
        
        attack_data_array = np.array(attack_data)
        x_attack = attack_data_array
        y_attack = np_utils.to_categorical(attack_y)
        # y_attack = attack_y
        x_attack = x_attack.astype('float32') / 255.0
        return(x_attack, y_attack)
    
    def load_84dcgan_attack_data(self, model):
        if(model == '1'):
            imsize = 28
        elif(model == '2'):
            imsize = 64
        elif(model == '3'):
            imsize = 75
        elif(model == '4'):
            imsize = 32
        attack_path = "./dataset/input_attack_data/84classes/"
        attack_folderpath = []
        attack_filefullpath = []
        attack_datalabel = []
        attack_data = []
        attack_y = []
        
        for attack_folder in os.listdir(attack_path):
            attack_folderpath.append(os.path.join(attack_path, attack_folder))
        for attack_catchfolderpath in attack_folderpath:
            for attack_file in os.listdir(attack_catchfolderpath):
                attack_filefullpath.append(os.path.join(attack_catchfolderpath, attack_file))
                attack_datalabel.append(attack_catchfolderpath.split('/')[4])
        
        attack_data_dict = {'attack_image_path': attack_filefullpath, 'attack_label': attack_datalabel}
        attack_my_data = pd.DataFrame(attack_data_dict)
        
        attack_classlabel = {'100rpm_1to475': 0,
                  '100rpm_476to1190': 1,
                  '100rpm_1191to3250': 2,
                  '100rpm_3251to5940': 3,
                  '100rpm_5941to8790': 4,
                  '100rpm_8791to10799': 5,
                  '200rpm_1to690': 6,
                  '200rpm_691to1640': 7,
                  '200rpm_1641to3220': 8,
                  '200rpm_3221to6185': 9,
                  '200rpm_6186to7915': 10,
                  '200rpm_7916to10799': 11,
                  '300rpm_1to775': 12,
                  '300rpm_776to1700': 13,
                  '300rpm_1701to3590': 14,
                  '300rpm_3591to6525': 15,
                  '300rpm_6526to8545': 16,
                  '300rpm_8546to10799': 17,
                  '400rpm_1to640': 18,
                  '400rpm_641to1385': 19,
                  '400rpm_1386to1900': 20,
                  '400rpm_1901to3960': 21,
                  '400rpm_3961to7100': 22,
                  '400rpm_7101to10799': 23,
                  '500rpm_1to690': 24,
                  '500rpm_691to1380': 25,
                  '500rpm_1381to2205': 26,
                  '500rpm_2206to3700': 27,
                  '500rpm_3701to7055': 28,
                  '500rpm_7056to10799': 29,
                  '600rpm_1to505': 30,
                  '600rpm_506to1070': 31,
                  '600rpm_1071to2135': 32,
                  '600rpm_2136to4625': 33,
                  '600rpm_4626to6325': 34,
                  '600rpm_6326to10799': 35,
                  '700rpm_1to750': 36,
                  '700rpm_751to1810': 37,
                  '700rpm_1811to3065': 38,
                  '700rpm_3066to6655': 39,
                  '700rpm_6656to8080': 40,
                  '700rpm_8081to10799': 41,
                  '800rpm_1to575': 42,
                  '800rpm_576to2110': 43,
                  '800rpm_2111to4560': 44,
                  '800rpm_4561to7655': 45,
                  '800rpm_7656to10799': 46,
                  '900rpm_1to580': 47,
                  '900rpm_581to1655': 48,
                  '900rpm_1656to3480': 49,
                  '900rpm_3481to6660': 50,
                  '900rpm_6661to8250': 51,
                  '900rpm_8251to10799': 52,
                  '1000rpm_1to800': 53,
                  '1000rpm_801to1720': 54,
                  '1000rpm_1721to3310': 55,
                  '1000rpm_3311to5175': 56,
                  '1000rpm_5176to8500': 57,
                  '1000rpm_8501to10799': 58,
                  '1100rpm_1to800': 59,
                  '1100rpm_801to2950': 60,
                  '1100rpm_2951to5510': 61,
                  '1100rpm_5511to8000': 62,
                  '1100rpm_8001to10799': 63,
                  '1200rpm_1to700': 64,
                  '1200rpm_701to2780': 65,
                  '1200rpm_2781to5910': 66,
                  '1200rpm_5911to7780': 67,
                  '1200rpm_7781to10799': 68,
                  '1300rpm_1to590': 69,
                  '1300rpm_591to2400': 70,
                  '1300rpm_2401to5710': 71,
                  '1300rpm_5711to8120': 72,
                  '1300rpm_8121to10799': 73,
                  '1400rpm_1to580': 74,
                  '1400rpm_581to2190': 75,
                  '1400rpm_2191to5900': 76,
                  '1400rpm_5901to8730': 77,
                  '1400rpm_8731to10799': 78,
                  '1500rpm_1to450': 79,
                  '1500rpm_451to1900': 80,
                  '1500rpm_1901to5200': 81,
                  '1500rpm_5201to8600': 82,
                  '1500rpm_8601to10799': 83}      
        
        attack_my_data['attack_label'] = attack_my_data['attack_label'].map(attack_classlabel)
        attack_my_data = attack_my_data.sort_values('attack_label')
        attack_my_data = attack_my_data.reset_index(drop=True)
        att_y = attack_my_data['attack_label']
        
        a = list(attack_my_data['attack_image_path']) 
        #print(att_y)
        attack_img_size = (imsize, imsize)
        
        for attack_i in range(len(a)):
            attack_img_bgr = cv2.imread(a[attack_i])
            attack_img_rgb = attack_img_bgr[:, :, ::-1]
            attack_img_fixsize = cv2.resize(attack_img_rgb, attack_img_size)
            attack_data.append(attack_img_fixsize)
            attack_y.append(att_y[attack_i])
        
        attack_data_array = np.array(attack_data)
        x_attack = attack_data_array
        y_attack = np_utils.to_categorical(attack_y)
        # y_attack = attack_y
        x_attack = x_attack.astype('float32') / 255.0
        return(x_attack, y_attack)