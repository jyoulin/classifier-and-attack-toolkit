import os
import warnings
warnings.filterwarnings("ignore")

from classifier.original.Original_CNN_15 import Original_CNN15
from classifier.original.Original_CNN_30 import Original_CNN30
from classifier.original.Original_CNN_60 import Original_CNN60
from classifier.original.Original_CNN_84 import Original_CNN84
from classifier.original.Original_CNN_84dcgan import Original_CNN84dcgan
from classifier.original.Original_CNN_135 import Original_CNN135
from classifier.original.Original_VGGNet_15 import Original_VGGNet15
from classifier.original.Original_VGGNet_30 import Original_VGGNet30
from classifier.original.Original_VGGNet_60 import Original_VGGNet60
from classifier.original.Original_VGGNet_84 import Original_VGGNet84
from classifier.original.Original_VGGNet_84dcgan import Original_VGGNet84dcgan
from classifier.original.Original_VGGNet_135 import Original_VGGNet135
from classifier.original.Original_Inception_15 import Original_Inception15
from classifier.original.Original_Inception_30 import Original_Inception30
from classifier.original.Original_Inception_60 import Original_Inception60
from classifier.original.Original_Inception_84 import Original_Inception84
from classifier.original.Original_Inception_84dcgan import Original_Inception84dcgan
from classifier.original.Original_Inception_135 import Original_Inception135
from classifier.original.Original_ResNet_15 import Original_ResNet15
from classifier.original.Original_ResNet_30 import Original_ResNet30
from classifier.original.Original_ResNet_60 import Original_ResNet60
from classifier.original.Original_ResNet_84 import Original_ResNet84
from classifier.original.Original_ResNet_84dcgan import Original_ResNet84dcgan
from classifier.original.Original_ResNet_135 import Original_ResNet135

from classifier.normal.Normal_CNN_16 import Normal_CNN16
from classifier.normal.Normal_CNN_31 import Normal_CNN31
from classifier.normal.Normal_CNN_61 import Normal_CNN61
from classifier.normal.Normal_CNN_85 import Normal_CNN85
from classifier.normal.Normal_CNN_85dcgan import Normal_CNN85dcgan
from classifier.normal.Normal_CNN_136 import Normal_CNN136
from classifier.normal.Normal_VGGNet_16 import Normal_VGGNet16
from classifier.normal.Normal_VGGNet_31 import Normal_VGGNet31
from classifier.normal.Normal_VGGNet_61 import Normal_VGGNet61
from classifier.normal.Normal_VGGNet_85 import Normal_VGGNet85
from classifier.normal.Normal_VGGNet_85dcgan import Normal_VGGNet85dcgan
from classifier.normal.Normal_VGGNet_136 import Normal_VGGNet136
from classifier.normal.Normal_Inception_16 import Normal_Inception16
from classifier.normal.Normal_Inception_31 import Normal_Inception31
from classifier.normal.Normal_Inception_61 import Normal_Inception61
from classifier.normal.Normal_Inception_85 import Normal_Inception85
from classifier.normal.Normal_Inception_85dcgan import Normal_Inception85dcgan
from classifier.normal.Normal_Inception_136 import Normal_Inception136
from classifier.normal.Normal_ResNet_16 import Normal_ResNet16
from classifier.normal.Normal_ResNet_31 import Normal_ResNet31
from classifier.normal.Normal_ResNet_61 import Normal_ResNet61
from classifier.normal.Normal_ResNet_85 import Normal_ResNet85
from classifier.normal.Normal_ResNet_85dcgan import Normal_ResNet85dcgan
from classifier.normal.Normal_ResNet_136 import Normal_ResNet136

from classifier.defense.Defense_CNN_16 import Defense_CNN16
from classifier.defense.Defense_CNN_31 import Defense_CNN31
from classifier.defense.Defense_CNN_61 import Defense_CNN61
from classifier.defense.Defense_CNN_85 import Defense_CNN85
from classifier.defense.Defense_CNN_85dcgan import Defense_CNN85dcgan
from classifier.defense.Defense_CNN_136 import Defense_CNN136
from classifier.defense.Defense_VGGNet_16 import Defense_VGGNet16
from classifier.defense.Defense_VGGNet_31 import Defense_VGGNet31
from classifier.defense.Defense_VGGNet_61 import Defense_VGGNet61
from classifier.defense.Defense_VGGNet_85 import Defense_VGGNet85
from classifier.defense.Defense_VGGNet_85dcgan import Defense_VGGNet85dcgan
from classifier.defense.Defense_VGGNet_136 import Defense_VGGNet136
from classifier.defense.Defense_Inception_16 import Defense_Inception16
from classifier.defense.Defense_Inception_31 import Defense_Inception31
from classifier.defense.Defense_Inception_61 import Defense_Inception61
from classifier.defense.Defense_Inception_85 import Defense_Inception85
from classifier.defense.Defense_Inception_85dcgan import Defense_Inception85dcgan
from classifier.defense.Defense_Inception_136 import Defense_Inception136
from classifier.defense.Defense_ResNet_16 import Defense_ResNet16
from classifier.defense.Defense_ResNet_31 import Defense_ResNet31
from classifier.defense.Defense_ResNet_61 import Defense_ResNet61
from classifier.defense.Defense_ResNet_85 import Defense_ResNet85
from classifier.defense.Defense_ResNet_85dcgan import Defense_ResNet85dcgan
from classifier.defense.Defense_ResNet_136 import Defense_ResNet136

class TestModel():
    def __init__(self, action):
        self.__action = action
        self.test_model(action)
    
    def test_dataset_show(self, action):
        print("\nThe dataset has the following...")
        print("(1) dataset 1: actual thermal image - 15 classes")
        print("(2) dataset 2: actual thermal image - 15+1(Others) classes")     # normal
        print("(3) dataset 3: actual thermal image - 15+1(DCGAN generate) classes")     # defense
        print("(4) dataset 4: actual thermal image - 30 classes")
        print("(5) dataset 5: actual thermal image - 30+1(Others) classes")     # normal
        print("(6) dataset 6: actual thermal image - 30+1(DCGAN generate) classes")     # defense
        print("(7) dataset 7: actual thermal image - 60 classes")
        print("(8) dataset 8: actual thermal image - 60+1(Others) classes")     # normal
        print("(9) dataset 9: actual thermal image - 60+1(DCGAN generate) classes")     # defense
        print("(10) dataset 10: simulated thermal image - 135 classes")
        print("(11) dataset 11: simulated thermal image - 135+1(Others) classes")     # normal
        print("(12) dataset 12: simulated thermal image - 135+1(DCGAN generate) classes")     # defense
        print("(13) dataset 13: simulated thermal image - 84 classes")
        print("(14) dataset 14: simulated thermal image - 84+1(Others) classes")     # normal
        print("(15) dataset 15: simulated thermal image - 84+1(DCGAN generate) classes")     # defense
        print("(16) dataset 16: simulated thermal image(DCGAN) - 84 classes")
        print("(17) dataset 17: simulated thermal image(DCGAN) - 84+1(Others) classes")     # normal
        print("(18) dataset 18: simulated thermal image(DCGAN) - 84+1(DCGAN generate) classes")     # defense


    def model_show(self):
        print("\nThe model has the following...")
        print("(1) Model 1: CNN")
        print("(2) Model 2: VGGNet")
        print("(3) Model 3: Inception-V3")
        print("(4) Model 4: ResNet")
   
    def test_model(self, action):
        m_gate = 0
        while(m_gate == 0):
            self.model_show()
            model = input("Please choose a model(1~4): ")
            os.system('cls')
            if model == '1':
                cnn_d_gate = 0
                while(cnn_d_gate == 0):
                    self.test_dataset_show(action)
                    dataset_cnn = input("Please choose a dataset(1~18): ")
                    os.system('cls')
                    if dataset_cnn == '1':
                        Original_CNN15()
                        cnn_d_gate = 1
                    elif dataset_cnn == '2':
                        Normal_CNN16()
                        cnn_d_gate = 1
                    elif dataset_cnn == '3':
                        Defense_CNN16()
                        cnn_d_gate = 1
                    elif dataset_cnn == '4':
                        Original_CNN30()
                        cnn_d_gate = 1
                    elif dataset_cnn == '5':
                        Normal_CNN31()
                        cnn_d_gate = 1
                    elif dataset_cnn == '6':
                        Defense_CNN31()
                        cnn_d_gate = 1
                    elif dataset_cnn == '7':
                        Original_CNN60()
                        cnn_d_gate = 1
                    elif dataset_cnn == '8':
                        Normal_CNN61()
                        cnn_d_gate = 1
                    elif dataset_cnn == '9':
                        Defense_CNN61()
                        cnn_d_gate = 1
                    elif dataset_cnn == '10':
                        Original_CNN135()
                        cnn_d_gate = 1
                    elif dataset_cnn == '11':
                        Normal_CNN136()
                        cnn_d_gate = 1
                    elif dataset_cnn == '12':
                        Defense_CNN136()
                        cnn_d_gate = 1
                    elif dataset_cnn == '13':
                        Original_CNN84()
                        cnn_d_gate = 1
                    elif dataset_cnn == '14':
                        Normal_CNN85()
                        cnn_d_gate = 1
                    elif dataset_cnn == '15':
                        Defense_CNN85()
                        cnn_d_gate = 1
                    elif dataset_cnn == '16':
                        Original_CNN84dcgan()
                        cnn_d_gate = 1
                    elif dataset_cnn == '17':
                        Normal_CNN85dcgan()
                        cnn_d_gate = 1
                    elif dataset_cnn == '18':
                        Defense_CNN85dcgan()
                        cnn_d_gate = 1
                m_gate = 1
                os.system('pause')
                          
            elif model == '2':
                vgg_d_gate = 0
                while(vgg_d_gate == 0):
                    self.test_dataset_show(action)
                    dataset_vggnet = input("Please choose a dataset(1~18): ")
                    os.system('cls')
                    if dataset_vggnet == '1':
                        Original_VGGNet15()
                        vgg_d_gate = 1
                    elif dataset_vggnet == '2':
                        Normal_VGGNet16()
                        vgg_d_gate = 1
                    elif dataset_vggnet == '3':
                        Defense_VGGNet16()
                        vgg_d_gate = 1
                    elif dataset_vggnet == '4':
                        Original_VGGNet30()
                        vgg_d_gate = 1
                    elif dataset_vggnet == '5':
                        Normal_VGGNet31()
                        vgg_d_gate = 1
                    elif dataset_vggnet == '6':
                        Defense_VGGNet31()
                        vgg_d_gate = 1
                    elif dataset_vggnet == '7':
                        Original_VGGNet60()
                        vgg_d_gate = 1
                    elif dataset_vggnet == '8':
                        Normal_VGGNet61()
                        vgg_d_gate = 1
                    elif dataset_vggnet == '9':
                        Defense_VGGNet61()
                        vgg_d_gate = 1
                    elif dataset_vggnet == '10':
                        Original_VGGNet135()
                        vgg_d_gate = 1
                    elif dataset_vggnet == '11':
                        Normal_VGGNet136()
                        vgg_d_gate = 1
                    elif dataset_vggnet == '12':
                        Defense_VGGNet136()
                        vgg_d_gate = 1
                    elif dataset_vggnet == '13':
                        Original_VGGNet84()
                        vgg_d_gate = 1
                    elif dataset_vggnet == '14':
                        Normal_VGGNet85()
                        vgg_d_gate = 1
                    elif dataset_vggnet == '15':
                        Defense_VGGNet85()
                        vgg_d_gate = 1
                    elif dataset_vggnet == '16':
                        Original_VGGNet84dcgan()
                        vgg_d_gate = 1
                    elif dataset_vggnet == '17':
                        Normal_VGGNet85dcgan()
                        vgg_d_gate = 1
                    elif dataset_vggnet == '18':
                        Defense_VGGNet85dcgan()
                        vgg_d_gate = 1
                m_gate = 1
                os.system('pause')
                
            elif model == '3':
                inception_d_gate = 0
                while(inception_d_gate == 0):
                    self.test_dataset_show(action)
                    dataset_inception = input("Please choose a dataset(1~18): ")
                    os.system('cls')
                    if dataset_inception == '1':
                        Original_Inception15()
                        inception_d_gate = 1
                    elif dataset_inception == '2':
                        Normal_Inception16()
                        inception_d_gate = 1
                    elif dataset_inception == '3':
                        Defense_Inception16()
                        inception_d_gate = 1
                    elif dataset_inception == '4':
                        Original_Inception30()
                        inception_d_gate = 1
                    elif dataset_inception == '5':
                        Normal_Inception31()
                        inception_d_gate = 1
                    elif dataset_inception == '6':
                        Defense_Inception31()
                        inception_d_gate = 1   
                    elif dataset_inception == '7':
                        Original_Inception60()
                        inception_d_gate = 1
                    elif dataset_inception == '8':
                        Normal_Inception61()
                        inception_d_gate = 1
                    elif dataset_inception == '9':
                        Defense_Inception61()
                        inception_d_gate = 1
                    elif dataset_inception == '10':
                        Original_Inception135()
                        inception_d_gate = 1
                    elif dataset_inception == '11':
                        Normal_Inception136()
                        inception_d_gate = 1
                    elif dataset_inception == '12':
                        Defense_Inception136()
                        inception_d_gate = 1
                    elif dataset_inception == '13':
                        Original_Inception84()
                        inception_d_gate = 1
                    elif dataset_inception == '14':
                        Normal_Inception85()
                        inception_d_gate = 1
                    elif dataset_inception == '15':
                        Defense_Inception85()
                        inception_d_gate = 1
                    elif dataset_inception == '16':
                        Original_Inception84dcgan()
                        inception_d_gate = 1
                    elif dataset_inception == '17':
                        Normal_Inception85dcgan()
                        inception_d_gate = 1
                    elif dataset_inception == '18':
                        Defense_Inception85dcgan()
                        inception_d_gate = 1
                m_gate = 1
                os.system('pause')
                
            elif model == '4':
                resnet_d_gate = 0
                while(resnet_d_gate == 0):
                    self.test_dataset_show(action)
                    dataset_resnet = input("Please choose a dataset(1~18): ")
                    os.system('cls')
                    if dataset_resnet == '1':
                        Original_ResNet15()
                        resnet_d_gate = 1
                    elif dataset_resnet == '2':
                        Normal_ResNet16()
                        resnet_d_gate = 1
                    elif dataset_resnet == '3':
                        Defense_ResNet16()
                        resnet_d_gate = 1
                    elif dataset_resnet == '4':
                        Original_ResNet30()
                        resnet_d_gate = 1
                    elif dataset_resnet == '5':
                        Normal_ResNet31()
                        resnet_d_gate = 1
                    elif dataset_resnet == '6':
                        Defense_ResNet31()
                        resnet_d_gate = 1
                    elif dataset_resnet == '7':
                        Original_ResNet60()
                        resnet_d_gate = 1
                    elif dataset_resnet == '8':
                        Normal_ResNet61()
                        resnet_d_gate = 1
                    elif dataset_resnet == '9':
                        Defense_ResNet61()
                        resnet_d_gate = 1
                    elif dataset_resnet == '10':
                        Original_ResNet135()
                        resnet_d_gate = 1
                    elif dataset_resnet == '11':
                        Normal_ResNet136()
                        resnet_d_gate = 1
                    elif dataset_resnet == '12':
                        Defense_ResNet136()
                        resnet_d_gate = 1
                    elif dataset_resnet == '13':
                        Original_ResNet84()
                        resnet_d_gate = 1
                    elif dataset_resnet == '14':
                        Normal_ResNet85()
                        resnet_d_gate = 1
                    elif dataset_resnet == '15':
                        Defense_ResNet85()
                        resnet_d_gate = 1
                    elif dataset_resnet == '16':
                        Original_ResNet84dcgan()
                        resnet_d_gate = 1
                    elif dataset_resnet == '17':
                        Normal_ResNet85dcgan()
                        resnet_d_gate = 1
                    elif dataset_resnet == '18':
                        Defense_ResNet85dcgan()
                        resnet_d_gate = 1
                m_gate = 1
                os.system('pause')