class Original_Inception60(): 
    def __init__(self):
        self.original_inception60()

    def original_inception60(self):
        import warnings
        import os
        import numpy as np
        import pandas as pd
        import cv2
        import matplotlib.pyplot as plt
        import tensorflow as tf
        from keras.utils import np_utils
        from keras.applications.inception_v3 import InceptionV3
        from keras.models import Model
        from keras.layers import Dense, Flatten
        from keras.optimizers import Adam
        from keras.backend import round, clip, sum, epsilon
        
        warnings.filterwarnings("ignore")
        tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
        os.environ['PYTHONHASHSEED']='0'
        os.environ['CUDA_VISIBLE_DEVICES']='0'
        os.environ['TF_CUDNN_USE_AUTOTUNE'] ='0'
        os.environ['KMP_WARNINGS'] = 'off'
        cfg = tf.ConfigProto() 
        cfg.gpu_options.allow_growth = True
        session = tf.Session(config=cfg)
        
        def cal_base(y_true, y_pred):
            y_pred_positive = round(clip(y_pred, 0, 1))
            y_pred_negative = 1 - y_pred_positive
            y_positive = round(clip(y_true, 0, 1))
            y_negative = 1 - y_positive
            TP = sum(y_positive * y_pred_positive)
            TN = sum(y_negative * y_pred_negative)
            FP = sum(y_negative * y_pred_positive)
            FN = sum(y_positive * y_pred_negative)
            return TP, TN, FP, FN
        
        def acc(y_true, y_pred):
            TP, TN, FP, FN = cal_base(y_true, y_pred)
            ACC = (TP + TN) / (TP + FP + FN + TN + epsilon())
            return ACC
        
        def sensitivity(y_true, y_pred):
            """ recall """
            TP, TN, FP, FN = cal_base(y_true, y_pred)
            SE = TP/(TP + FN + epsilon())
            return SE
        
        def precision(y_true, y_pred):
            TP, TN, FP, FN = cal_base(y_true, y_pred)
            PC = TP/(TP + FP + epsilon())
            return PC
        
        def specificity(y_true, y_pred):
            TP, TN, FP, FN = cal_base(y_true, y_pred)
            SP = TN / (TN + FP + epsilon())
            return SP
        
        def f1_score(y_true, y_pred):
            SE = sensitivity(y_true, y_pred)
            PC = precision(y_true, y_pred)
            F1 = 2 * SE * PC / (SE + PC + epsilon())
            return F1
        
        imsize = 224
        labelcount = 60
        
        path='./dataset/original_data/train_60'
        folderpath = []
        filefullpath = []
        datalabel = []
        train_data = []
        train_y = []
        
        for folder in os.listdir(path):
            folderpath.append(os.path.join(path, folder))
        for catchfolderpath in folderpath:
            for file in os.listdir(catchfolderpath):
                filefullpath.append(os.path.join(catchfolderpath, file))
                datalabel.append(catchfolderpath.split('/')[4])
        
        data = {'image_path': filefullpath, 'label': datalabel}
        my_data = pd.DataFrame(data)
        
        classlabel = {'500rpm_0to68s': 0,
                      '500rpm_69to137s': 1,
                      '500rpm_138to206s': 2,
                      '500rpm_207to275s': 3,
                      '500rpm_276to344s': 4,
                      '500rpm_345to413s': 5,
                      '500rpm_414to482s': 6,
                      '500rpm_483to551s': 7,
                      '500rpm_552to620s': 8,
                      '500rpm_621to689s': 9,
                      '500rpm_690to758s': 10,
                      '500rpm_759to827s': 11,
                      '500rpm_828to896s': 12,
                      '500rpm_897to965s': 13,
                      '500rpm_966to1034s': 14,
                      '500rpm_1035to1103s': 15,
                      '500rpm_1104to1172s': 16,
                      '500rpm_1173to1241s': 17,
                      '500rpm_1242to1310s': 18,
                      '500rpm_1311to1379s': 19,
                      '500rpm_1380to1448s': 20,
                      '500rpm_1449to1517s': 21,
                      '500rpm_1518to1586s': 22,
                      '500rpm_1587to1655s': 23,
                      '500rpm_1656to1724s': 24,
                      '500rpm_1725to1793s': 25,
                      '1000rpm_0to62s': 26,
                      '1000rpm_63to125s': 27,
                      '1000rpm_126to188s': 28,
                      '1000rpm_189to251s': 29,
                      '1000rpm_252to314s': 30,
                      '1000rpm_315to377s': 31,
                      '1000rpm_378to440s': 32,
                      '1000rpm_441to503s': 33,
                      '1000rpm_504to566s': 34,
                      '1000rpm_567to629s': 35,
                      '1000rpm_630to692s': 36,
                      '1000rpm_693to755s': 37,
                      '1000rpm_756to818s': 38,
                      '1000rpm_819to881s': 39,
                      '1000rpm_882to944s': 40,
                      '1000rpm_945to1007s': 41,
                      '1000rpm_1008to1070s': 42,
                      '1000rpm_1071to1133s': 43,
                      '1000rpm_1134to1196s': 44,
                      '1500rpm_0to59s': 45,
                      '1500rpm_60to119s': 46,
                      '1500rpm_120to179s': 47,
                      '1500rpm_180to239s': 48,
                      '1500rpm_240to299s': 49,
                      '1500rpm_300to359s': 50,
                      '1500rpm_360to419s': 51,
                      '1500rpm_420to479s': 52,
                      '1500rpm_480to539s': 53,
                      '1500rpm_540to599s': 54,
                      '1500rpm_600to659s': 55,
                      '1500rpm_660to719s': 56,
                      '1500rpm_720to779s': 57,
                      '1500rpm_780to839s': 58,
                      '1500rpm_840to899s': 59}
        
        my_data['label'] = my_data['label'].map(classlabel)
        my_data = my_data.sort_values('label')
        my_data = my_data.reset_index(drop=True)
        y = my_data['label']
        #add
        a = my_data['image_path']
        a = list(my_data['image_path'])        
        img_size = (imsize, imsize)

        for i in range(len(a)):
            img_bgr = cv2.imread(a[i])
            img_rgb = img_bgr[:, :, ::-1]
            img_fixsize = cv2.resize(img_rgb, img_size)
            train_data.append(img_fixsize)
            train_y.append(y[i])
        
        train_data_array = np.array(train_data)
        
        val_path='./dataset/original_data/val_60'
        val_folderpath = []
        val_filefullpath = []
        val_datalabel = []
        val_data = []
        val_y = []

        for val_folder in os.listdir(val_path):
            val_folderpath.append(os.path.join(val_path, val_folder))
        for val_catchfolderpath in val_folderpath:
            for val_file in os.listdir(val_catchfolderpath):
                val_filefullpath.append(os.path.join(val_catchfolderpath, val_file))
                val_datalabel.append(val_catchfolderpath.split('/')[4])

        val_data_dict = {'val_image_path': val_filefullpath, 'val_label': val_datalabel}
        val_my_data = pd.DataFrame(val_data_dict)

        val_classlabel = {'500rpm_0to68s': 0,
                          '500rpm_69to137s': 1,
                          '500rpm_138to206s': 2,
                          '500rpm_207to275s': 3,
                          '500rpm_276to344s': 4,
                          '500rpm_345to413s': 5,
                          '500rpm_414to482s': 6,
                          '500rpm_483to551s': 7,
                          '500rpm_552to620s': 8,
                          '500rpm_621to689s': 9,
                          '500rpm_690to758s': 10,
                          '500rpm_759to827s': 11,
                          '500rpm_828to896s': 12,
                          '500rpm_897to965s': 13,
                          '500rpm_966to1034s': 14,
                          '500rpm_1035to1103s': 15,
                          '500rpm_1104to1172s': 16,
                          '500rpm_1173to1241s': 17,
                          '500rpm_1242to1310s': 18,
                          '500rpm_1311to1379s': 19,
                          '500rpm_1380to1448s': 20,
                          '500rpm_1449to1517s': 21,
                          '500rpm_1518to1586s': 22,
                          '500rpm_1587to1655s': 23,
                          '500rpm_1656to1724s': 24,
                          '500rpm_1725to1793s': 25,
                          '1000rpm_0to62s': 26,
                          '1000rpm_63to125s': 27,
                          '1000rpm_126to188s': 28,
                          '1000rpm_189to251s': 29,
                          '1000rpm_252to314s': 30,
                          '1000rpm_315to377s': 31,
                          '1000rpm_378to440s': 32,
                          '1000rpm_441to503s': 33,
                          '1000rpm_504to566s': 34,
                          '1000rpm_567to629s': 35,
                          '1000rpm_630to692s': 36,
                          '1000rpm_693to755s': 37,
                          '1000rpm_756to818s': 38,
                          '1000rpm_819to881s': 39,
                          '1000rpm_882to944s': 40,
                          '1000rpm_945to1007s': 41,
                          '1000rpm_1008to1070s': 42,
                          '1000rpm_1071to1133s': 43,
                          '1000rpm_1134to1196s': 44,
                          '1500rpm_0to59s': 45,
                          '1500rpm_60to119s': 46,
                          '1500rpm_120to179s': 47,
                          '1500rpm_180to239s': 48,
                          '1500rpm_240to299s': 49,
                          '1500rpm_300to359s': 50,
                          '1500rpm_360to419s': 51,
                          '1500rpm_420to479s': 52,
                          '1500rpm_480to539s': 53,
                          '1500rpm_540to599s': 54,
                          '1500rpm_600to659s': 55,
                          '1500rpm_660to719s': 56,
                          '1500rpm_720to779s': 57,
                          '1500rpm_780to839s': 58,
                          '1500rpm_840to899s': 59}

        val_my_data['val_label'] = val_my_data['val_label'].map(val_classlabel)
        val_my_data = val_my_data.sort_values('val_label')
        val_my_data = val_my_data.reset_index(drop=True)
        v_y = val_my_data['val_label']
        #add
        a = val_my_data['val_image_path']
        a = list(val_my_data['val_image_path'])
        val_img_size = (imsize, imsize)

        for val_i in range(len(a)):
            val_img_bgr = cv2.imread(a[val_i])
            val_img_rgb = val_img_bgr[:, :, ::-1]
            val_img_fixsize = cv2.resize(val_img_rgb, val_img_size)
            val_data.append(val_img_fixsize)
            val_y.append(v_y[val_i])

        val_data_array = np.array(val_data)
        
        test_path='./dataset/original_data/test_60'
        test_folderpath = []
        test_filefullpath = []
        test_datalabel = []
        test_data = []
        test_y = []
        
        for test_folder in os.listdir(test_path):
            test_folderpath.append(os.path.join(test_path, test_folder))
        for test_catchfolderpath in test_folderpath:
            for test_file in os.listdir(test_catchfolderpath):
                test_filefullpath.append(os.path.join(test_catchfolderpath, test_file))
                test_datalabel.append(test_catchfolderpath.split('/')[4])
        
        test_data_dict = {'test_image_path': test_filefullpath, 'test_label': test_datalabel}
        test_my_data = pd.DataFrame(test_data_dict)
        
        test_classlabel = {'500rpm_0to68s': 0,
                           '500rpm_69to137s': 1,
                           '500rpm_138to206s': 2,
                           '500rpm_207to275s': 3,
                           '500rpm_276to344s': 4,
                           '500rpm_345to413s': 5,
                           '500rpm_414to482s': 6,
                           '500rpm_483to551s': 7,
                           '500rpm_552to620s': 8,
                           '500rpm_621to689s': 9,
                           '500rpm_690to758s': 10,
                           '500rpm_759to827s': 11,
                           '500rpm_828to896s': 12,
                           '500rpm_897to965s': 13,
                           '500rpm_966to1034s': 14,
                           '500rpm_1035to1103s': 15,
                           '500rpm_1104to1172s': 16,
                           '500rpm_1173to1241s': 17,
                           '500rpm_1242to1310s': 18,
                           '500rpm_1311to1379s': 19,
                           '500rpm_1380to1448s': 20,
                           '500rpm_1449to1517s': 21,
                           '500rpm_1518to1586s': 22,
                           '500rpm_1587to1655s': 23,
                           '500rpm_1656to1724s': 24,
                           '500rpm_1725to1793s': 25,
                           '1000rpm_0to62s': 26,
                           '1000rpm_63to125s': 27,
                           '1000rpm_126to188s': 28,
                           '1000rpm_189to251s': 29,
                           '1000rpm_252to314s': 30,
                           '1000rpm_315to377s': 31,
                           '1000rpm_378to440s': 32,
                           '1000rpm_441to503s': 33,
                           '1000rpm_504to566s': 34,
                           '1000rpm_567to629s': 35,
                           '1000rpm_630to692s': 36,
                           '1000rpm_693to755s': 37,
                           '1000rpm_756to818s': 38,
                           '1000rpm_819to881s': 39,
                           '1000rpm_882to944s': 40,
                           '1000rpm_945to1007s': 41,
                           '1000rpm_1008to1070s': 42,
                           '1000rpm_1071to1133s': 43,
                           '1000rpm_1134to1196s': 44,
                           '1500rpm_0to59s': 45,
                           '1500rpm_60to119s': 46,
                           '1500rpm_120to179s': 47,
                           '1500rpm_180to239s': 48,
                           '1500rpm_240to299s': 49,
                           '1500rpm_300to359s': 50,
                           '1500rpm_360to419s': 51,
                           '1500rpm_420to479s': 52,
                           '1500rpm_480to539s': 53,
                           '1500rpm_540to599s': 54,
                           '1500rpm_600to659s': 55,
                           '1500rpm_660to719s': 56,
                           '1500rpm_720to779s': 57,
                           '1500rpm_780to839s': 58,
                           '1500rpm_840to899s': 59}
        
        test_my_data['test_label'] = test_my_data['test_label'].map(test_classlabel)
        test_my_data = test_my_data.sort_values('test_label')
        test_my_data = test_my_data.reset_index(drop=True)
        te_y = test_my_data['test_label']
        #add
        a = test_my_data['test_image_path']
        a = list(test_my_data['test_image_path'])        
        test_img_size = (imsize, imsize)

        for test_i in range(len(a)):
            test_img_bgr = cv2.imread(a[test_i])
            test_img_rgb = test_img_bgr[:, :, ::-1]
            test_img_fixsize = cv2.resize(test_img_rgb, test_img_size)
            test_data.append(test_img_fixsize)
            test_y.append(te_y[test_i])
        
        test_data_array = np.array(test_data)
        
        x_train = train_data_array
        x_val = val_data_array
        x_test = test_data_array
        y_train = np_utils.to_categorical(train_y)
        y_val = np_utils.to_categorical(val_y)
        y_test = np_utils.to_categorical(test_y)
        x_train = x_train.astype('float32') / 255.0
        x_val = x_val.astype('float32') / 255.0
        x_test = x_test.astype('float32') / 255.0
        
        model = InceptionV3(include_top=False, weights='imagenet', input_shape=(imsize, imsize, 3))
        flat = Flatten()(model.layers[-1].output)
        dense = Dense(128, activation='relu')(flat)
        dense = Dense(128, activation='relu')(dense)
        dense = Dense(64, activation='relu')(dense)
        output = Dense(labelcount, activation='softmax')(dense)
        model = Model(inputs=model.inputs, outputs=output)
        
        for layer in model.layers:
            layer.trainable = True
        
        callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=8, mode='min')
        model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.000004), metrics=['accuracy', sensitivity, precision, specificity, f1_score]) 
        
        epoch = 200
        batch_size = 16
        train_history=model.fit(x=x_train, y=y_train, validation_data = (x_val, y_val), epochs=epoch, batch_size=batch_size, verbose=1, callbacks=[callback])
        
        model.save('./model/Original_Inception60_model.h5')
        model.save_weights('./model/Original_Inception60_weight.h5')
        
        train_acc_list = []
        val_acc_list = []
        def show_train_history_acc(train_acc, val_acc):
            plt.figure(1)
            
            plt.plot(train_history.history[train_acc])            
            train_acc_list.extend(train_history.history[train_acc])
            plt.plot(train_history.history[val_acc])
            val_acc_list.extend(train_history.history[val_acc])
            
            plt.title('Learning curve')
            plt.ylabel('Accuracy')
            plt.xlabel('Epoch')
            
            plt.legend(['train', 'val'], loc='upper left')
            plt.savefig('./learning_curve/Original_Inception60_acc.png')
        show_train_history_acc('accuracy', 'val_accuracy')

        train_loss_list = []
        val_loss_list = []
        def show_train_history_loss(train_loss, val_loss):
            plt.figure(2)
            
            plt.plot(train_history.history[train_loss])
            train_loss_list.extend(train_history.history[train_loss])
            plt.plot(train_history.history[val_loss])
            val_loss_list.extend(train_history.history[val_loss])
            
            plt.title('Learning curve')
            plt.ylabel('Loss')
            plt.xlabel('Epoch')
            
            plt.legend(['train', 'val'], loc='upper left')
            plt.savefig('./learning_curve/Original_Inception60_loss.png')
        show_train_history_loss('loss', 'val_loss')
        
        # print("Accuracy =", val_acc_list[len(val_acc_list)-1])
        
        scores = model.evaluate(x_test, y_test, verbose=2)
        print("Test Accuracy = ", scores[1]) 
        # print("Test Loss: = ", scores[0])
        
        predict = model.predict(x_test)
        predict_classes = np.argmax(predict,axis=1)
        # print("Label = ", predict_classes)