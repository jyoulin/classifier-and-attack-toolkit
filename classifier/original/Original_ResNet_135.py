class Original_ResNet135(): 
    def __init__(self):
        self.original_resnet135()

    def original_resnet135(self):
        import warnings
        import os
        import numpy as np
        import pandas as pd
        import cv2
        import matplotlib.pyplot as plt
        import tensorflow as tf
        from keras.utils import np_utils
        from keras.applications.resnet50 import ResNet50
        from keras.models import Model
        from keras.layers import Dense, Flatten
        from keras.optimizers import Adam
        from keras.backend import round, clip, sum, epsilon
        
        warnings.filterwarnings("ignore")
        tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
        os.environ['PYTHONHASHSEED']='0'
        os.environ['CUDA_VISIBLE_DEVICES']='0'
        os.environ['TF_CUDNN_USE_AUTOTUNE'] ='0'
        os.environ['KMP_WARNINGS'] = 'off'
        cfg = tf.ConfigProto() 
        cfg.gpu_options.allow_growth = True
        session = tf.Session(config=cfg)

        def cal_base(y_true, y_pred):
            y_pred_positive = round(clip(y_pred, 0, 1))
            y_pred_negative = 1 - y_pred_positive
            y_positive = round(clip(y_true, 0, 1))
            y_negative = 1 - y_positive
            TP = sum(y_positive * y_pred_positive)
            TN = sum(y_negative * y_pred_negative)
            FP = sum(y_negative * y_pred_positive)
            FN = sum(y_positive * y_pred_negative)
            return TP, TN, FP, FN

        def acc(y_true, y_pred):
            TP, TN, FP, FN = cal_base(y_true, y_pred)
            ACC = (TP + TN) / (TP + FP + FN + TN + epsilon())
            return ACC

        def sensitivity(y_true, y_pred):
            """ recall """
            TP, TN, FP, FN = cal_base(y_true, y_pred)
            SE = TP/(TP + FN + epsilon())
            return SE

        def precision(y_true, y_pred):
            TP, TN, FP, FN = cal_base(y_true, y_pred)
            PC = TP/(TP + FP + epsilon())
            return PC

        def specificity(y_true, y_pred):
            TP, TN, FP, FN = cal_base(y_true, y_pred)
            SP = TN / (TN + FP + epsilon())
            return SP

        def f1_score(y_true, y_pred):
            SE = sensitivity(y_true, y_pred)
            PC = precision(y_true, y_pred)
            F1 = 2 * SE * PC / (SE + PC + epsilon())
            return F1

        imsize = 32
        labelcount = 135

        path='./dataset/original_data/train_135'
        folderpath = []
        filefullpath = []
        datalabel = []
        train_data = []
        train_y = []

        for folder in os.listdir(path):
            folderpath.append(os.path.join(path, folder))
        for catchfolderpath in folderpath:
            for file in os.listdir(catchfolderpath):
                filefullpath.append(os.path.join(catchfolderpath, file))
                datalabel.append(catchfolderpath.split('/')[4])
                
        data = {'image_path': filefullpath, 'label': datalabel}
        my_data = pd.DataFrame(data)

        classlabel = {'100rpm_90to1108': 0, '100rpm_1198to2216': 1, '100rpm_2306to3324': 2, '100rpm_3414to4432': 3, '100rpm_4522to5540': 4, '100rpm_5630to6648': 5, '100rpm_6738to7756': 6, '100rpm_7846to8864': 7, '100rpm_8954to9972': 8,
                      '200rpm_90to1108': 9, '200rpm_1198to2216': 10, '200rpm_2306to3324': 11, '200rpm_3414to4432': 12, '200rpm_4522to5540': 13, '200rpm_5630to6648': 14, '200rpm_6738to7756': 15, '200rpm_7846to8864': 16, '200rpm_8954to9972': 17,
                      '300rpm_90to1108': 18, '300rpm_1198to2216': 19, '300rpm_2306to3324': 20, '300rpm_3414to4432': 21, '300rpm_4522to5540': 22, '300rpm_5630to6648': 23, '300rpm_6738to7756': 24, '300rpm_7846to8864': 25, '300rpm_8954to9972': 26,
                      '400rpm_90to1108': 27, '400rpm_1198to2216': 28, '400rpm_2306to3324': 29, '400rpm_3414to4432': 30, '400rpm_4522to5540': 31, '400rpm_5630to6648': 32, '400rpm_6738to7756': 33, '400rpm_7846to8864': 34, '400rpm_8954to9972': 35,
                      '500rpm_90to1108': 36, '500rpm_1198to2216': 37, '500rpm_2306to3324': 38, '500rpm_3414to4432': 39, '500rpm_4522to5540': 40, '500rpm_5630to6648': 41, '500rpm_6738to7756': 42, '500rpm_7846to8864': 43, '500rpm_8954to9972': 44,
                      '600rpm_90to1108': 45, '600rpm_1198to2216': 46, '600rpm_2306to3324': 47, '600rpm_3414to4432': 48, '600rpm_4522to5540': 49, '600rpm_5630to6648': 50, '600rpm_6738to7756': 51, '600rpm_7846to8864': 52, '600rpm_8954to9972': 53,
                      '700rpm_90to1108': 54, '700rpm_1198to2216': 55, '700rpm_2306to3324': 56, '700rpm_3414to4432': 57, '700rpm_4522to5540': 58, '700rpm_5630to6648': 59, '700rpm_6738to7756': 60, '700rpm_7846to8864': 61, '700rpm_8954to9972': 62,
                      '800rpm_90to1108': 63, '800rpm_1198to2216': 64, '800rpm_2306to3324': 65, '800rpm_3414to4432': 66, '800rpm_4522to5540': 67, '800rpm_5630to6648': 68, '800rpm_6738to7756': 69, '800rpm_7846to8864': 70, '800rpm_8954to9972': 71,
                      '900rpm_90to1108': 72, '900rpm_1198to2216': 73, '900rpm_2306to3324': 74, '900rpm_3414to4432': 75, '900rpm_4522to5540': 76, '900rpm_5630to6648': 77, '900rpm_6738to7756': 78, '900rpm_7846to8864': 79, '900rpm_8954to9972': 80,
                      '1000rpm_90to1108': 81, '1000rpm_1198to2216': 82, '1000rpm_2306to3324': 83, '1000rpm_3414to4432': 84, '1000rpm_4522to5540': 85, '1000rpm_5630to6648': 86, '1000rpm_6738to7756': 87, '1000rpm_7846to8864': 88, '1000rpm_8954to9972': 89,
                      '1100rpm_90to1108': 90, '1100rpm_1198to2216': 91, '1100rpm_2306to3324': 92, '1100rpm_3414to4432': 93, '1100rpm_4522to5540': 94, '1100rpm_5630to6648': 95, '1100rpm_6738to7756': 96, '1100rpm_7846to8864': 97, '1100rpm_8954to9972': 98,
                      '1200rpm_90to1108': 99, '1200rpm_1198to2216': 100, '1200rpm_2306to3324': 101, '1200rpm_3414to4432': 102, '1200rpm_4522to5540': 103, '1200rpm_5630to6648': 104, '1200rpm_6738to7756': 105, '1200rpm_7846to8864': 106, '1200rpm_8954to9972': 107,
                      '1300rpm_90to1108': 108, '1300rpm_1198to2216': 109, '1300rpm_2306to3324': 110, '1300rpm_3414to4432': 111, '1300rpm_4522to5540': 112, '1300rpm_5630to6648': 113, '1300rpm_6738to7756': 114, '1300rpm_7846to8864': 115, '1300rpm_8954to9972': 116,
                      '1400rpm_90to1108': 117, '1400rpm_1198to2216': 118, '1400rpm_2306to3324': 119, '1400rpm_3414to4432': 120, '1400rpm_4522to5540': 121, '1400rpm_5630to6648': 122, '1400rpm_6738to7756': 123, '1400rpm_7846to8864': 124, '1400rpm_8954to9972': 125,
                      '1500rpm_90to1108': 126, '1500rpm_1198to2216': 127, '1500rpm_2306to3324': 128, '1500rpm_3414to4432': 129, '1500rpm_4522to5540': 130, '1500rpm_5630to6648': 131, '1500rpm_6738to7756': 132, '1500rpm_7846to8864': 133, '1500rpm_8954to9972': 134}

        my_data['label'] = my_data['label'].map(classlabel)
        my_data = my_data.sort_values('label')
        my_data = my_data.reset_index(drop=True)
        y = my_data['label']
        #add
        a = my_data['image_path']
        a = list(my_data['image_path'])        
        img_size = (imsize, imsize)

        for i in range(len(a)):
            img_bgr = cv2.imread(a[i])
            img_rgb = img_bgr[:, :, ::-1]
            img_fixsize = cv2.resize(img_rgb, img_size)
            train_data.append(img_fixsize)
            train_y.append(y[i])

        train_data_array = np.array(train_data)
        
        val_path='./dataset/original_data/val_135'
        val_folderpath = []
        val_filefullpath = []
        val_datalabel = []
        val_data = []
        val_y = []

        for val_folder in os.listdir(val_path):
            val_folderpath.append(os.path.join(val_path, val_folder))
        for val_catchfolderpath in val_folderpath:
            for val_file in os.listdir(val_catchfolderpath):
                val_filefullpath.append(os.path.join(val_catchfolderpath, val_file))
                val_datalabel.append(val_catchfolderpath.split('/')[4])

        val_data_dict = {'val_image_path': val_filefullpath, 'val_label': val_datalabel}
        val_my_data = pd.DataFrame(val_data_dict)

        val_classlabel = {'100rpm_90to1108': 0, '100rpm_1198to2216': 1, '100rpm_2306to3324': 2, '100rpm_3414to4432': 3, '100rpm_4522to5540': 4, '100rpm_5630to6648': 5, '100rpm_6738to7756': 6, '100rpm_7846to8864': 7, '100rpm_8954to9972': 8,
                          '200rpm_90to1108': 9, '200rpm_1198to2216': 10, '200rpm_2306to3324': 11, '200rpm_3414to4432': 12, '200rpm_4522to5540': 13, '200rpm_5630to6648': 14, '200rpm_6738to7756': 15, '200rpm_7846to8864': 16, '200rpm_8954to9972': 17,
                          '300rpm_90to1108': 18, '300rpm_1198to2216': 19, '300rpm_2306to3324': 20, '300rpm_3414to4432': 21, '300rpm_4522to5540': 22, '300rpm_5630to6648': 23, '300rpm_6738to7756': 24, '300rpm_7846to8864': 25, '300rpm_8954to9972': 26,
                          '400rpm_90to1108': 27, '400rpm_1198to2216': 28, '400rpm_2306to3324': 29, '400rpm_3414to4432': 30, '400rpm_4522to5540': 31, '400rpm_5630to6648': 32, '400rpm_6738to7756': 33, '400rpm_7846to8864': 34, '400rpm_8954to9972': 35,
                          '500rpm_90to1108': 36, '500rpm_1198to2216': 37, '500rpm_2306to3324': 38, '500rpm_3414to4432': 39, '500rpm_4522to5540': 40, '500rpm_5630to6648': 41, '500rpm_6738to7756': 42, '500rpm_7846to8864': 43, '500rpm_8954to9972': 44,
                          '600rpm_90to1108': 45, '600rpm_1198to2216': 46, '600rpm_2306to3324': 47, '600rpm_3414to4432': 48, '600rpm_4522to5540': 49, '600rpm_5630to6648': 50, '600rpm_6738to7756': 51, '600rpm_7846to8864': 52, '600rpm_8954to9972': 53,
                          '700rpm_90to1108': 54, '700rpm_1198to2216': 55, '700rpm_2306to3324': 56, '700rpm_3414to4432': 57, '700rpm_4522to5540': 58, '700rpm_5630to6648': 59, '700rpm_6738to7756': 60, '700rpm_7846to8864': 61, '700rpm_8954to9972': 62,
                          '800rpm_90to1108': 63, '800rpm_1198to2216': 64, '800rpm_2306to3324': 65, '800rpm_3414to4432': 66, '800rpm_4522to5540': 67, '800rpm_5630to6648': 68, '800rpm_6738to7756': 69, '800rpm_7846to8864': 70, '800rpm_8954to9972': 71,
                          '900rpm_90to1108': 72, '900rpm_1198to2216': 73, '900rpm_2306to3324': 74, '900rpm_3414to4432': 75, '900rpm_4522to5540': 76, '900rpm_5630to6648': 77, '900rpm_6738to7756': 78, '900rpm_7846to8864': 79, '900rpm_8954to9972': 80,
                          '1000rpm_90to1108': 81, '1000rpm_1198to2216': 82, '1000rpm_2306to3324': 83, '1000rpm_3414to4432': 84, '1000rpm_4522to5540': 85, '1000rpm_5630to6648': 86, '1000rpm_6738to7756': 87, '1000rpm_7846to8864': 88, '1000rpm_8954to9972': 89,
                          '1100rpm_90to1108': 90, '1100rpm_1198to2216': 91, '1100rpm_2306to3324': 92, '1100rpm_3414to4432': 93, '1100rpm_4522to5540': 94, '1100rpm_5630to6648': 95, '1100rpm_6738to7756': 96, '1100rpm_7846to8864': 97, '1100rpm_8954to9972': 98,
                          '1200rpm_90to1108': 99, '1200rpm_1198to2216': 100, '1200rpm_2306to3324': 101, '1200rpm_3414to4432': 102, '1200rpm_4522to5540': 103, '1200rpm_5630to6648': 104, '1200rpm_6738to7756': 105, '1200rpm_7846to8864': 106, '1200rpm_8954to9972': 107,
                          '1300rpm_90to1108': 108, '1300rpm_1198to2216': 109, '1300rpm_2306to3324': 110, '1300rpm_3414to4432': 111, '1300rpm_4522to5540': 112, '1300rpm_5630to6648': 113, '1300rpm_6738to7756': 114, '1300rpm_7846to8864': 115, '1300rpm_8954to9972': 116,
                          '1400rpm_90to1108': 117, '1400rpm_1198to2216': 118, '1400rpm_2306to3324': 119, '1400rpm_3414to4432': 120, '1400rpm_4522to5540': 121, '1400rpm_5630to6648': 122, '1400rpm_6738to7756': 123, '1400rpm_7846to8864': 124, '1400rpm_8954to9972': 125,
                          '1500rpm_90to1108': 126, '1500rpm_1198to2216': 127, '1500rpm_2306to3324': 128, '1500rpm_3414to4432': 129, '1500rpm_4522to5540': 130, '1500rpm_5630to6648': 131, '1500rpm_6738to7756': 132, '1500rpm_7846to8864': 133, '1500rpm_8954to9972': 134}

        val_my_data['val_label'] = val_my_data['val_label'].map(val_classlabel)
        val_my_data = val_my_data.sort_values('val_label')
        val_my_data = val_my_data.reset_index(drop=True)
        v_y = val_my_data['val_label']
        #add
        a = val_my_data['val_image_path']
        a = list(val_my_data['val_image_path'])
        val_img_size = (imsize, imsize)

        for val_i in range(len(a)):
            val_img_bgr = cv2.imread(a[val_i])
            val_img_rgb = val_img_bgr[:, :, ::-1]
            val_img_fixsize = cv2.resize(val_img_rgb, val_img_size)
            val_data.append(val_img_fixsize)
            val_y.append(v_y[val_i])

        val_data_array = np.array(val_data)

        test_path='./dataset/original_data/test_135'
        test_folderpath = []
        test_filefullpath = []
        test_datalabel = []
        test_data = []
        test_y = []

        for test_folder in os.listdir(test_path):
            test_folderpath.append(os.path.join(test_path, test_folder))
        for test_catchfolderpath in test_folderpath:
            for test_file in os.listdir(test_catchfolderpath):
                test_filefullpath.append(os.path.join(test_catchfolderpath, test_file))
                test_datalabel.append(test_catchfolderpath.split('/')[4])

        test_data_dict = {'test_image_path': test_filefullpath, 'test_label': test_datalabel}
        test_my_data = pd.DataFrame(test_data_dict)

        test_classlabel = {'100rpm_90to1108': 0, '100rpm_1198to2216': 1, '100rpm_2306to3324': 2, '100rpm_3414to4432': 3, '100rpm_4522to5540': 4, '100rpm_5630to6648': 5, '100rpm_6738to7756': 6, '100rpm_7846to8864': 7, '100rpm_8954to9972': 8,
                           '200rpm_90to1108': 9, '200rpm_1198to2216': 10, '200rpm_2306to3324': 11, '200rpm_3414to4432': 12, '200rpm_4522to5540': 13, '200rpm_5630to6648': 14, '200rpm_6738to7756': 15, '200rpm_7846to8864': 16, '200rpm_8954to9972': 17,
                           '300rpm_90to1108': 18, '300rpm_1198to2216': 19, '300rpm_2306to3324': 20, '300rpm_3414to4432': 21, '300rpm_4522to5540': 22, '300rpm_5630to6648': 23, '300rpm_6738to7756': 24, '300rpm_7846to8864': 25, '300rpm_8954to9972': 26,
                           '400rpm_90to1108': 27, '400rpm_1198to2216': 28, '400rpm_2306to3324': 29, '400rpm_3414to4432': 30, '400rpm_4522to5540': 31, '400rpm_5630to6648': 32, '400rpm_6738to7756': 33, '400rpm_7846to8864': 34, '400rpm_8954to9972': 35,
                           '500rpm_90to1108': 36, '500rpm_1198to2216': 37, '500rpm_2306to3324': 38, '500rpm_3414to4432': 39, '500rpm_4522to5540': 40, '500rpm_5630to6648': 41, '500rpm_6738to7756': 42, '500rpm_7846to8864': 43, '500rpm_8954to9972': 44,
                           '600rpm_90to1108': 45, '600rpm_1198to2216': 46, '600rpm_2306to3324': 47, '600rpm_3414to4432': 48, '600rpm_4522to5540': 49, '600rpm_5630to6648': 50, '600rpm_6738to7756': 51, '600rpm_7846to8864': 52, '600rpm_8954to9972': 53,
                           '700rpm_90to1108': 54, '700rpm_1198to2216': 55, '700rpm_2306to3324': 56, '700rpm_3414to4432': 57, '700rpm_4522to5540': 58, '700rpm_5630to6648': 59, '700rpm_6738to7756': 60, '700rpm_7846to8864': 61, '700rpm_8954to9972': 62,
                           '800rpm_90to1108': 63, '800rpm_1198to2216': 64, '800rpm_2306to3324': 65, '800rpm_3414to4432': 66, '800rpm_4522to5540': 67, '800rpm_5630to6648': 68, '800rpm_6738to7756': 69, '800rpm_7846to8864': 70, '800rpm_8954to9972': 71,
                           '900rpm_90to1108': 72, '900rpm_1198to2216': 73, '900rpm_2306to3324': 74, '900rpm_3414to4432': 75, '900rpm_4522to5540': 76, '900rpm_5630to6648': 77, '900rpm_6738to7756': 78, '900rpm_7846to8864': 79, '900rpm_8954to9972': 80,
                           '1000rpm_90to1108': 81, '1000rpm_1198to2216': 82, '1000rpm_2306to3324': 83, '1000rpm_3414to4432': 84, '1000rpm_4522to5540': 85, '1000rpm_5630to6648': 86, '1000rpm_6738to7756': 87, '1000rpm_7846to8864': 88, '1000rpm_8954to9972': 89,
                           '1100rpm_90to1108': 90, '1100rpm_1198to2216': 91, '1100rpm_2306to3324': 92, '1100rpm_3414to4432': 93, '1100rpm_4522to5540': 94, '1100rpm_5630to6648': 95, '1100rpm_6738to7756': 96, '1100rpm_7846to8864': 97, '1100rpm_8954to9972': 98,
                           '1200rpm_90to1108': 99, '1200rpm_1198to2216': 100, '1200rpm_2306to3324': 101, '1200rpm_3414to4432': 102, '1200rpm_4522to5540': 103, '1200rpm_5630to6648': 104, '1200rpm_6738to7756': 105, '1200rpm_7846to8864': 106, '1200rpm_8954to9972': 107,
                           '1300rpm_90to1108': 108, '1300rpm_1198to2216': 109, '1300rpm_2306to3324': 110, '1300rpm_3414to4432': 111, '1300rpm_4522to5540': 112, '1300rpm_5630to6648': 113, '1300rpm_6738to7756': 114, '1300rpm_7846to8864': 115, '1300rpm_8954to9972': 116,
                           '1400rpm_90to1108': 117, '1400rpm_1198to2216': 118, '1400rpm_2306to3324': 119, '1400rpm_3414to4432': 120, '1400rpm_4522to5540': 121, '1400rpm_5630to6648': 122, '1400rpm_6738to7756': 123, '1400rpm_7846to8864': 124, '1400rpm_8954to9972': 125,
                           '1500rpm_90to1108': 126, '1500rpm_1198to2216': 127, '1500rpm_2306to3324': 128, '1500rpm_3414to4432': 129, '1500rpm_4522to5540': 130, '1500rpm_5630to6648': 131, '1500rpm_6738to7756': 132, '1500rpm_7846to8864': 133, '1500rpm_8954to9972': 134}

        test_my_data['test_label'] = test_my_data['test_label'].map(test_classlabel)
        test_my_data = test_my_data.sort_values('test_label')
        test_my_data = test_my_data.reset_index(drop=True)
        te_y = test_my_data['test_label']
        #add
        a = test_my_data['test_image_path']
        a = list(test_my_data['test_image_path'])        
        test_img_size = (imsize, imsize)

        for test_i in range(len(a)):
            test_img_bgr = cv2.imread(a[test_i])
            test_img_rgb = test_img_bgr[:, :, ::-1]
            test_img_fixsize = cv2.resize(test_img_rgb, test_img_size)
            test_data.append(test_img_fixsize)
            test_y.append(te_y[test_i])

        test_data_array = np.array(test_data)

        x_train = train_data_array
        x_val = val_data_array
        x_test = test_data_array
        y_train = np_utils.to_categorical(train_y)
        y_val = np_utils.to_categorical(val_y)
        y_test = np_utils.to_categorical(test_y)
        x_train = x_train.astype('float32') / 255.0
        x_val = x_val.astype('float32') / 255.0
        x_test = x_test.astype('float32') / 255.0

        model = ResNet50(include_top=False, weights='imagenet', input_shape=(imsize, imsize, 3))
        flat = Flatten()(model.layers[-1].output)
        dense = Dense(512, activation='relu')(flat)
        dense = Dense(256, activation='relu')(dense)
        output = Dense(labelcount, activation='softmax')(dense)
        model = Model(inputs=model.inputs, outputs=output)

        for layer in model.layers:
            layer.trainable = True
            
        callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=2, mode='min')
        model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.00001), metrics=['accuracy', sensitivity, precision, specificity, f1_score]) 

        epoch = 100
        batch_size = 16
        train_history=model.fit(x=x_train, y=y_train, validation_data = (x_val, y_val), epochs=epoch, batch_size=batch_size, verbose=1, callbacks=[callback])

        model.save('./model/Original_ResNet135_model.h5')
        model.save_weights('./model/Original_ResNet135_weight.h5')

        train_acc_list = []
        val_acc_list = []
        def show_train_history_acc(train_acc, val_acc):
            plt.figure(1)
            
            plt.plot(train_history.history[train_acc])            
            train_acc_list.extend(train_history.history[train_acc])
            plt.plot(train_history.history[val_acc])
            val_acc_list.extend(train_history.history[val_acc])
            
            plt.title('Learning curve')
            plt.ylabel('Accuracy')
            plt.xlabel('Epoch')
            
            plt.legend(['train', 'val'], loc='upper left')
            plt.savefig('./learning_curve/Original_ResNet135_acc.png')
        show_train_history_acc('accuracy', 'val_accuracy')

        train_loss_list = []
        val_loss_list = []
        def show_train_history_loss(train_loss, val_loss):
            plt.figure(2)
            
            plt.plot(train_history.history[train_loss])
            train_loss_list.extend(train_history.history[train_loss])
            plt.plot(train_history.history[val_loss])
            val_loss_list.extend(train_history.history[val_loss])
            
            plt.title('Learning curve')
            plt.ylabel('Loss')
            plt.xlabel('Epoch')
            
            plt.legend(['train', 'val'], loc='upper left')
            plt.savefig('./learning_curve/Original_ResNet135_loss.png')
        show_train_history_loss('loss', 'val_loss')
        
        # print("Accuracy =", val_acc_list[len(val_acc_list)-1])
        
        scores = model.evaluate(x_test, y_test, verbose=2)
        print("Test Accuracy = ", scores[1]) 
        # print("Test Loss: = ", scores[0])
        
        predict = model.predict(x_test)
        predict_classes = np.argmax(predict,axis=1)
        # print("Label = ", predict_classes)