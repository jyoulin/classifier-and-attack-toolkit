class Defense_Inception85dcgan(): 
    def __init__(self):
        self.defense_inception85dcgan()

    def defense_inception85dcgan(self):
        import warnings
        import os
        import numpy as np
        import pandas as pd
        import cv2
        import matplotlib.pyplot as plt
        import tensorflow as tf
        from keras.utils import np_utils
        from keras.applications.inception_v3 import InceptionV3
        from keras.models import Model
        from keras.layers import Dense, Flatten
        from keras.optimizers import Adam
        from keras.backend import round, clip, sum, epsilon
        
        warnings.filterwarnings("ignore")
        tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
        os.environ['PYTHONHASHSEED']='0'
        os.environ['CUDA_VISIBLE_DEVICES']='0'
        os.environ['TF_CUDNN_USE_AUTOTUNE'] ='0'
        os.environ['KMP_WARNINGS'] = 'off'
        cfg = tf.ConfigProto() 
        cfg.gpu_options.allow_growth = True
        session = tf.Session(config=cfg)

        def cal_base(y_true, y_pred):
            y_pred_positive = round(clip(y_pred, 0, 1))
            y_pred_negative = 1 - y_pred_positive
            y_positive = round(clip(y_true, 0, 1))
            y_negative = 1 - y_positive
            TP = sum(y_positive * y_pred_positive)
            TN = sum(y_negative * y_pred_negative)
            FP = sum(y_negative * y_pred_positive)
            FN = sum(y_positive * y_pred_negative)
            return TP, TN, FP, FN

        def acc(y_true, y_pred):
            TP, TN, FP, FN = cal_base(y_true, y_pred)
            ACC = (TP + TN) / (TP + FP + FN + TN + epsilon())
            return ACC

        def sensitivity(y_true, y_pred):
            """ recall """
            TP, TN, FP, FN = cal_base(y_true, y_pred)
            SE = TP/(TP + FN + epsilon())
            return SE

        def precision(y_true, y_pred):
            TP, TN, FP, FN = cal_base(y_true, y_pred)
            PC = TP/(TP + FP + epsilon())
            return PC

        def specificity(y_true, y_pred):
            TP, TN, FP, FN = cal_base(y_true, y_pred)
            SP = TN / (TN + FP + epsilon())
            return SP

        def f1_score(y_true, y_pred):
            SE = sensitivity(y_true, y_pred)
            PC = precision(y_true, y_pred)
            F1 = 2 * SE * PC / (SE + PC + epsilon())
            return F1

        imsize = 75
        labelcount = 85

        path='./dataset/defense_data/train_85dcgan'
        folderpath = []
        filefullpath = []
        datalabel = []
        train_data = []
        train_y = []

        for folder in os.listdir(path):
            folderpath.append(os.path.join(path, folder))
        for catchfolderpath in folderpath:
            for file in os.listdir(catchfolderpath):
                filefullpath.append(os.path.join(catchfolderpath, file))
                datalabel.append(catchfolderpath.split('/')[4] )

        data = {'image_path': filefullpath, 'label': datalabel}
        my_data = pd.DataFrame(data)

        classlabel = {'100rpm_1to475': 0, '100rpm_476to1190': 1, '100rpm_1191to3250': 2, '100rpm_3251to5940': 3, '100rpm_5941to8790': 4, '100rpm_8791to10799': 5, 
                      '200rpm_1to690': 6, '200rpm_691to1640': 7, '200rpm_1641to3220': 8, '200rpm_3221to6185': 9, '200rpm_6186to7915': 10,'200rpm_7916to10799': 11, 
                      '300rpm_1to775': 12, '300rpm_776to1700': 13, '300rpm_1701to3590': 14, '300rpm_3591to6525': 15, '300rpm_6526to8545': 16, '300rpm_8546to10799': 17, 
                      '400rpm_1to640': 18, '400rpm_641to1385': 19, '400rpm_1386to1900': 20,'400rpm_1901to3960': 21, '400rpm_3961to7100': 22, '400rpm_7101to10799': 23, 
                      '500rpm_1to690': 24, '500rpm_691to1380': 25, '500rpm_1381to2205': 26, '500rpm_2206to3700': 27, '500rpm_3701to7055': 28, '500rpm_7056to10799': 29, 
                      '600rpm_1to505': 30, '600rpm_506to1070': 31, '600rpm_1071to2135': 32, '600rpm_2136to4625': 33, '600rpm_4626to6325': 34, '600rpm_6326to10799': 35, 
                      '700rpm_1to750': 36, '700rpm_751to1810': 37, '700rpm_1811to3065': 38, '700rpm_3066to6655': 39, '700rpm_6656to8080': 40, '700rpm_8081to10799': 41, 
                      '800rpm_1to575': 42, '800rpm_576to2110': 43, '800rpm_2111to4560': 44, '800rpm_4561to7655': 45, '800rpm_7656to10799': 46, 
                      '900rpm_1to580': 47, '900rpm_581to1655': 48, '900rpm_1656to3480': 49, '900rpm_3481to6660': 50, '900rpm_6661to8250': 51, '900rpm_8251to10799': 52, 
                      '1000rpm_1to800': 53, '1000rpm_801to1720': 54,'1000rpm_1721to3310': 55, '1000rpm_3311to5175': 56, '1000rpm_5176to8500': 57, '1000rpm_8501to10799': 58, 
                      '1100rpm_1to800': 59, '1100rpm_801to2950': 60, '1100rpm_2951to5510': 61, '1100rpm_5511to8000': 62, '1100rpm_8001to10799': 63, 
                      '1200rpm_1to700': 64, '1200rpm_701to2780': 65, '1200rpm_2781to5910': 66,'1200rpm_5911to7780': 67, '1200rpm_7781to10799': 68, 
                      '1300rpm_1to590': 69, '1300rpm_591to2400': 70, '1300rpm_2401to5710': 71, '1300rpm_5711to8120': 72, '1300rpm_8121to10799': 73, 
                      '1400rpm_1to580': 74, '1400rpm_581to2190': 75, '1400rpm_2191to5900': 76, '1400rpm_5901to8730': 77,'1400rpm_8731to10799': 78, 
                      '1500rpm_1to450': 79, '1500rpm_451to1900': 80, '1500rpm_1901to5200': 81, '1500rpm_5201to8600': 82, '1500rpm_8601to10799': 83,
                      'Defense': 84}

        my_data['label'] = my_data['label'].map(classlabel)
        my_data = my_data.sort_values('label')
        my_data = my_data.reset_index(drop=True)
        y = my_data['label']
        #add
        a = my_data['image_path']
        a = list(my_data['image_path'])        
        img_size = (imsize, imsize)

        for i in range(len(a)):
            img_bgr = cv2.imread(a[i])
            img_rgb = img_bgr[:, :, ::-1]
            img_fixsize = cv2.resize(img_rgb, img_size)
            train_data.append(img_fixsize)
            train_y.append(y[i])

        train_data_array = np.array(train_data)
        
        val_path='./dataset/defense_data/val_85dcgan'
        val_folderpath = []
        val_filefullpath = []
        val_datalabel = []
        val_data = []
        val_y = []

        for val_folder in os.listdir(val_path):
            val_folderpath.append(os.path.join(val_path, val_folder))
        for val_catchfolderpath in val_folderpath:
            for val_file in os.listdir(val_catchfolderpath):
                val_filefullpath.append(os.path.join(val_catchfolderpath, val_file))
                val_datalabel.append(val_catchfolderpath.split('/')[4])

        val_data_dict = {'val_image_path': val_filefullpath, 'val_label': val_datalabel}
        val_my_data = pd.DataFrame(val_data_dict)

        val_classlabel = {'100rpm_1to475': 0, '100rpm_476to1190': 1, '100rpm_1191to3250': 2, '100rpm_3251to5940': 3, '100rpm_5941to8790': 4, '100rpm_8791to10799': 5, 
                          '200rpm_1to690': 6, '200rpm_691to1640': 7, '200rpm_1641to3220': 8, '200rpm_3221to6185': 9, '200rpm_6186to7915': 10,'200rpm_7916to10799': 11, 
                          '300rpm_1to775': 12, '300rpm_776to1700': 13, '300rpm_1701to3590': 14, '300rpm_3591to6525': 15, '300rpm_6526to8545': 16, '300rpm_8546to10799': 17, 
                          '400rpm_1to640': 18, '400rpm_641to1385': 19, '400rpm_1386to1900': 20,'400rpm_1901to3960': 21, '400rpm_3961to7100': 22, '400rpm_7101to10799': 23, 
                          '500rpm_1to690': 24, '500rpm_691to1380': 25, '500rpm_1381to2205': 26, '500rpm_2206to3700': 27, '500rpm_3701to7055': 28, '500rpm_7056to10799': 29, 
                          '600rpm_1to505': 30, '600rpm_506to1070': 31, '600rpm_1071to2135': 32, '600rpm_2136to4625': 33, '600rpm_4626to6325': 34, '600rpm_6326to10799': 35, 
                          '700rpm_1to750': 36, '700rpm_751to1810': 37, '700rpm_1811to3065': 38, '700rpm_3066to6655': 39, '700rpm_6656to8080': 40, '700rpm_8081to10799': 41, 
                          '800rpm_1to575': 42, '800rpm_576to2110': 43, '800rpm_2111to4560': 44, '800rpm_4561to7655': 45, '800rpm_7656to10799': 46, 
                          '900rpm_1to580': 47, '900rpm_581to1655': 48, '900rpm_1656to3480': 49, '900rpm_3481to6660': 50, '900rpm_6661to8250': 51, '900rpm_8251to10799': 52, 
                          '1000rpm_1to800': 53, '1000rpm_801to1720': 54,'1000rpm_1721to3310': 55, '1000rpm_3311to5175': 56, '1000rpm_5176to8500': 57, '1000rpm_8501to10799': 58, 
                          '1100rpm_1to800': 59, '1100rpm_801to2950': 60, '1100rpm_2951to5510': 61, '1100rpm_5511to8000': 62, '1100rpm_8001to10799': 63, 
                          '1200rpm_1to700': 64, '1200rpm_701to2780': 65, '1200rpm_2781to5910': 66,'1200rpm_5911to7780': 67, '1200rpm_7781to10799': 68, 
                          '1300rpm_1to590': 69, '1300rpm_591to2400': 70, '1300rpm_2401to5710': 71, '1300rpm_5711to8120': 72, '1300rpm_8121to10799': 73, 
                          '1400rpm_1to580': 74, '1400rpm_581to2190': 75, '1400rpm_2191to5900': 76, '1400rpm_5901to8730': 77,'1400rpm_8731to10799': 78, 
                          '1500rpm_1to450': 79, '1500rpm_451to1900': 80, '1500rpm_1901to5200': 81, '1500rpm_5201to8600': 82, '1500rpm_8601to10799': 83,
                          'Defense': 84}

        val_my_data['val_label'] = val_my_data['val_label'].map(val_classlabel)
        val_my_data = val_my_data.sort_values('val_label')
        val_my_data = val_my_data.reset_index(drop=True)
        v_y = val_my_data['val_label']
        #add
        a = val_my_data['val_image_path']
        a = list(val_my_data['val_image_path'])
        val_img_size = (imsize, imsize)

        for val_i in range(len(a)):
            val_img_bgr = cv2.imread(a[val_i])
            val_img_rgb = val_img_bgr[:, :, ::-1]
            val_img_fixsize = cv2.resize(val_img_rgb, val_img_size)
            val_data.append(val_img_fixsize)
            val_y.append(v_y[val_i])

        val_data_array = np.array(val_data)
        
        test_path='./dataset/defense_data/test_85dcgan'
        test_folderpath = []
        test_filefullpath = []
        test_datalabel = []
        test_data = []
        test_y = []

        for test_folder in os.listdir(test_path):
            test_folderpath.append(os.path.join(test_path, test_folder))
        for test_catchfolderpath in test_folderpath:
            for test_file in os.listdir(test_catchfolderpath):
                test_filefullpath.append(os.path.join(test_catchfolderpath, test_file))
                test_datalabel.append(test_catchfolderpath.split('/')[4] )

        test_data_dict = {'test_image_path': test_filefullpath, 'test_label': test_datalabel}
        test_my_data = pd.DataFrame(test_data_dict)

        test_classlabel = {'100rpm_1to475': 0, '100rpm_476to1190': 1, '100rpm_1191to3250': 2, '100rpm_3251to5940': 3, '100rpm_5941to8790': 4, '100rpm_8791to10799': 5, 
                           '200rpm_1to690': 6, '200rpm_691to1640': 7, '200rpm_1641to3220': 8, '200rpm_3221to6185': 9, '200rpm_6186to7915': 10,'200rpm_7916to10799': 11, 
                           '300rpm_1to775': 12, '300rpm_776to1700': 13, '300rpm_1701to3590': 14, '300rpm_3591to6525': 15, '300rpm_6526to8545': 16, '300rpm_8546to10799': 17, 
                           '400rpm_1to640': 18, '400rpm_641to1385': 19, '400rpm_1386to1900': 20,'400rpm_1901to3960': 21, '400rpm_3961to7100': 22, '400rpm_7101to10799': 23, 
                           '500rpm_1to690': 24, '500rpm_691to1380': 25, '500rpm_1381to2205': 26, '500rpm_2206to3700': 27, '500rpm_3701to7055': 28, '500rpm_7056to10799': 29, 
                           '600rpm_1to505': 30, '600rpm_506to1070': 31, '600rpm_1071to2135': 32, '600rpm_2136to4625': 33, '600rpm_4626to6325': 34, '600rpm_6326to10799': 35, 
                           '700rpm_1to750': 36, '700rpm_751to1810': 37, '700rpm_1811to3065': 38, '700rpm_3066to6655': 39, '700rpm_6656to8080': 40, '700rpm_8081to10799': 41, 
                           '800rpm_1to575': 42, '800rpm_576to2110': 43, '800rpm_2111to4560': 44, '800rpm_4561to7655': 45, '800rpm_7656to10799': 46, 
                           '900rpm_1to580': 47, '900rpm_581to1655': 48, '900rpm_1656to3480': 49, '900rpm_3481to6660': 50, '900rpm_6661to8250': 51, '900rpm_8251to10799': 52, 
                           '1000rpm_1to800': 53, '1000rpm_801to1720': 54,'1000rpm_1721to3310': 55, '1000rpm_3311to5175': 56, '1000rpm_5176to8500': 57, '1000rpm_8501to10799': 58, 
                           '1100rpm_1to800': 59, '1100rpm_801to2950': 60, '1100rpm_2951to5510': 61, '1100rpm_5511to8000': 62, '1100rpm_8001to10799': 63, 
                           '1200rpm_1to700': 64, '1200rpm_701to2780': 65, '1200rpm_2781to5910': 66,'1200rpm_5911to7780': 67, '1200rpm_7781to10799': 68, 
                           '1300rpm_1to590': 69, '1300rpm_591to2400': 70, '1300rpm_2401to5710': 71, '1300rpm_5711to8120': 72, '1300rpm_8121to10799': 73, 
                           '1400rpm_1to580': 74, '1400rpm_581to2190': 75, '1400rpm_2191to5900': 76, '1400rpm_5901to8730': 77,'1400rpm_8731to10799': 78, 
                           '1500rpm_1to450': 79, '1500rpm_451to1900': 80, '1500rpm_1901to5200': 81, '1500rpm_5201to8600': 82, '1500rpm_8601to10799': 83,
                           'Defense': 84}

        test_my_data['test_label'] = test_my_data['test_label'].map(test_classlabel)
        test_my_data = test_my_data.sort_values('test_label')
        test_my_data = test_my_data.reset_index(drop=True)
        te_y = test_my_data['test_label']
        #add
        a = test_my_data['test_image_path']
        a = list(test_my_data['test_image_path'])        
        test_img_size = (imsize, imsize)

        for test_i in range(len(a)):
            test_img_bgr = cv2.imread(a[test_i])
            test_img_rgb = test_img_bgr[:, :, ::-1]
            test_img_fixsize = cv2.resize(test_img_rgb, test_img_size)
            test_data.append(test_img_fixsize)
            test_y.append(te_y[test_i])

        test_data_array = np.array(test_data)

        x_train = train_data_array
        x_val = val_data_array
        x_test = test_data_array
        y_train = np_utils.to_categorical(train_y)
        y_val = np_utils.to_categorical(val_y)
        y_test = np_utils.to_categorical(test_y)
        x_train = x_train.astype('float32') / 255.0
        x_val = x_val.astype('float32') / 255.0
        x_test = x_test.astype('float32') / 255.0

        model = InceptionV3(include_top=False, weights='imagenet', input_shape=(imsize, imsize, 3))
        flat = Flatten()(model.layers[-1].output)
        dense = Dense(512, activation='relu')(flat)
        dense = Dense(256, activation='relu')(dense)
        dense = Dense(128, activation='relu')(dense)
        output = Dense(labelcount, activation='softmax')(dense)
        model = Model(inputs=model.inputs, outputs=output)

        for layer in model.layers:
            layer.trainable = True

        callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=5, mode='min')
        model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.000005), metrics=['accuracy', sensitivity, precision, specificity, f1_score]) 

        epoch = 100
        batch_size = 16
        train_history=model.fit(x=x_train, y=y_train, validation_data = (x_val, y_val), epochs=epoch, batch_size=batch_size, verbose=1, callbacks=[callback])
              
        model.save('./model/Defense_Inception85dcgan_model.h5')
        model.save_weights('./model/Defense_Inception85dcgan_weight.h5')

        train_acc_list = []
        val_acc_list = []
        def show_train_history_acc(train_acc, val_acc):
            plt.figure(1)
            
            plt.plot(train_history.history[train_acc])            
            train_acc_list.extend(train_history.history[train_acc])
            plt.plot(train_history.history[val_acc])
            val_acc_list.extend(train_history.history[val_acc])
            
            plt.title('Learning curve')
            plt.ylabel('Accuracy')
            plt.xlabel('Epoch')
            
            plt.legend(['train', 'val'], loc='upper left')
            plt.savefig('./learning_curve/Defense_Inception85dcgan_acc.png')
        show_train_history_acc('accuracy', 'val_accuracy')

        train_loss_list = []
        val_loss_list = []
        def show_train_history_loss(train_loss, val_loss):
            plt.figure(2)
            
            plt.plot(train_history.history[train_loss])
            train_loss_list.extend(train_history.history[train_loss])
            plt.plot(train_history.history[val_loss])
            val_loss_list.extend(train_history.history[val_loss])
            
            plt.title('Learning curve')
            plt.ylabel('Loss')
            plt.xlabel('Epoch')
            
            plt.legend(['train', 'val'], loc='upper left')
            plt.savefig('./learning_curve/Defense_Inception85dcgan_loss.png')
        show_train_history_loss('loss', 'val_loss')
        
        # print("Accuracy =", val_acc_list[len(val_acc_list)-1])
        
        scores = model.evaluate(x_test, y_test, verbose=2)
        print("Test Accuracy = ", scores[1]) 
        # print("Test Loss: = ", scores[0])
        
        predict = model.predict(x_test)
        predict_classes = np.argmax(predict,axis=1)
        # print("Label = ", predict_classes)