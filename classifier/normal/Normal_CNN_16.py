class Normal_CNN16(): 
    def __init__(self):
        self.normal_cnn16()

    def normal_cnn16(self):
        import warnings
        import os
        import numpy as np
        import pandas as pd
        import cv2
        import matplotlib.pyplot as plt
        import tensorflow as tf
        from keras.utils import np_utils
        from keras.models import Sequential
        from keras.layers import Dense, Flatten, Conv2D, MaxPooling2D
        from keras.optimizers import Adam
        from keras.backend import round, clip, sum, epsilon
        
        warnings.filterwarnings("ignore")
        tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
        os.environ['PYTHONHASHSEED']='0'
        os.environ['CUDA_VISIBLE_DEVICES']='0'
        os.environ['TF_CUDNN_USE_AUTOTUNE'] ='0'
        os.environ['KMP_WARNINGS'] = 'off'
        cfg = tf.ConfigProto()
        cfg.gpu_options.allow_growth = True
        session = tf.Session(config=cfg)

        def cal_base(y_true, y_pred):
            y_pred_positive = round(clip(y_pred, 0, 1))
            y_pred_negative = 1 - y_pred_positive
            y_positive = round(clip(y_true, 0, 1))
            y_negative = 1 - y_positive
            TP = sum(y_positive * y_pred_positive)
            TN = sum(y_negative * y_pred_negative)
            FP = sum(y_negative * y_pred_positive)
            FN = sum(y_positive * y_pred_negative)
            return TP, TN, FP, FN

        def acc(y_true, y_pred):
            TP, TN, FP, FN = cal_base(y_true, y_pred)
            ACC = (TP + TN) / (TP + FP + FN + TN + epsilon())
            return ACC

        def sensitivity(y_true, y_pred):
            """ recall """
            TP, TN, FP, FN = cal_base(y_true, y_pred)
            SE = TP/(TP + FN + epsilon())
            return SE

        def precision(y_true, y_pred):
            TP, TN, FP, FN = cal_base(y_true, y_pred)
            PC = TP/(TP + FP + epsilon())
            return PC

        def specificity(y_true, y_pred):
            TP, TN, FP, FN = cal_base(y_true, y_pred)
            SP = TN / (TN + FP + epsilon())
            return SP

        def f1_score(y_true, y_pred):
            SE = sensitivity(y_true, y_pred)
            PC = precision(y_true, y_pred)
            F1 = 2 * SE * PC / (SE + PC + epsilon())
            return F1

        imsize = 224
        labelcount = 16

        path='./dataset/normal_data/train_16'
        folderpath = []
        filefullpath = []
        datalabel = []
        train_data = []
        train_y = []

        for folder in os.listdir(path):
            folderpath.append(os.path.join(path, folder))
        for catchfolderpath in folderpath:
            for file in os.listdir(catchfolderpath):
                filefullpath.append(os.path.join(catchfolderpath, file))
                datalabel.append(catchfolderpath.split('/')[4])

        data = {'image_path': filefullpath, 'label': datalabel}
        my_data = pd.DataFrame(data)

        classlabel = {'500rpm_16to345': 0,
                      '500rpm_376to705': 1,
                      '500rpm_736to1065': 2,
                      '500rpm_1096to1425': 3,
                      '500rpm_1456to1785': 4,
                      '1000rpm_16to225': 5,
                      '1000rpm_256to465': 6,
                      '1000rpm_496to705': 7,
                      '1000rpm_736to945': 8,
                      '1000rpm_976to1185': 9,
                      '1500rpm_16to165': 10,
                      '1500rpm_196to345': 11,
                      '1500rpm_376to525': 12,
                      '1500rpm_556to705': 13,
                      '1500rpm_736to885': 14,
                      'Normal': 15}

        my_data['label'] = my_data['label'].map(classlabel)
        my_data = my_data.sort_values('label')
        my_data = my_data.reset_index(drop=True)
        y = my_data['label']
        #add
        a = my_data['image_path']
        a = list(my_data['image_path'])        
        img_size = (imsize, imsize)

        for i in range(len(a)):
            img_bgr = cv2.imread(a[i])
            img_rgb = img_bgr[:, :, ::-1]
            img_fixsize = cv2.resize(img_rgb, img_size)
            train_data.append(img_fixsize)
            train_y.append(y[i])

        train_data_array = np.array(train_data)
        
        val_path='./dataset/normal_data/val_16'
        val_folderpath = []
        val_filefullpath = []
        val_datalabel = []
        val_data = []
        val_y = []

        for val_folder in os.listdir(val_path):
            val_folderpath.append(os.path.join(val_path, val_folder))
        for val_catchfolderpath in val_folderpath:
            for val_file in os.listdir(val_catchfolderpath):
                val_filefullpath.append(os.path.join(val_catchfolderpath, val_file))
                val_datalabel.append(val_catchfolderpath.split('/')[4])

        val_data_dict = {'val_image_path': val_filefullpath, 'val_label': val_datalabel}
        val_my_data = pd.DataFrame(val_data_dict)

        val_classlabel = {'500rpm_16to345': 0,
                          '500rpm_376to705': 1,
                          '500rpm_736to1065': 2,
                          '500rpm_1096to1425': 3,
                          '500rpm_1456to1785': 4,
                          '1000rpm_16to225': 5,
                          '1000rpm_256to465': 6,
                          '1000rpm_496to705': 7,
                          '1000rpm_736to945': 8,
                          '1000rpm_976to1185': 9,
                          '1500rpm_16to165': 10,
                          '1500rpm_196to345': 11,
                          '1500rpm_376to525': 12,
                          '1500rpm_556to705': 13,
                          '1500rpm_736to885': 14,
                          'Normal': 15}

        val_my_data['val_label'] = val_my_data['val_label'].map(val_classlabel)
        val_my_data = val_my_data.sort_values('val_label')
        val_my_data = val_my_data.reset_index(drop=True)
        v_y = val_my_data['val_label']
        #add
        a = val_my_data['val_image_path']
        a = list(val_my_data['val_image_path'])
        val_img_size = (imsize, imsize)
        
        for val_i in range(len(a)):
            val_img_bgr = cv2.imread(a[val_i])
            val_img_rgb = val_img_bgr[:, :, ::-1]
            val_img_fixsize = cv2.resize(val_img_rgb, val_img_size)
            val_data.append(val_img_fixsize)
            val_y.append(v_y[val_i])

        val_data_array = np.array(val_data)
        
        test_path='./dataset/normal_data/test_16'
        test_folderpath = []
        test_filefullpath = []
        test_datalabel = []
        test_data = []
        test_y = []

        for test_folder in os.listdir(test_path):
            test_folderpath.append(os.path.join(test_path, test_folder))
        for test_catchfolderpath in test_folderpath:
            for test_file in os.listdir(test_catchfolderpath):
                test_filefullpath.append(os.path.join(test_catchfolderpath, test_file))
                test_datalabel.append(test_catchfolderpath.split('/')[4])

        test_data_dict = {'test_image_path': test_filefullpath, 'test_label': test_datalabel}

        test_my_data = pd.DataFrame(test_data_dict)

        test_classlabel = {'500rpm_16to345': 0,
                           '500rpm_376to705': 1,
                           '500rpm_736to1065': 2,
                           '500rpm_1096to1425': 3,
                           '500rpm_1456to1785': 4,
                           '1000rpm_16to225': 5,
                           '1000rpm_256to465': 6,
                           '1000rpm_496to705': 7,
                           '1000rpm_736to945': 8,
                           '1000rpm_976to1185': 9,
                           '1500rpm_16to165': 10,
                           '1500rpm_196to345': 11,
                           '1500rpm_376to525': 12,
                           '1500rpm_556to705': 13,
                           '1500rpm_736to885': 14,
                           'Normal': 15}

        test_my_data['test_label'] = test_my_data['test_label'].map(test_classlabel)
        test_my_data = test_my_data.sort_values('test_label')
        test_my_data = test_my_data.reset_index(drop=True)
        te_y = test_my_data['test_label']
        #add
        a = test_my_data['test_image_path']
        a = list(test_my_data['test_image_path'])        
        test_img_size = (imsize, imsize)

        for test_i in range(len(a)):
            test_img_bgr = cv2.imread(a[test_i])
            test_img_rgb = test_img_bgr[:, :, ::-1]
            test_img_fixsize = cv2.resize(test_img_rgb, test_img_size)
            test_data.append(test_img_fixsize)
            test_y.append(te_y[test_i])

        test_data_array = np.array(test_data)

        x_train = train_data_array
        x_val = val_data_array
        x_test = test_data_array
        y_train = np_utils.to_categorical(train_y)
        y_val = np_utils.to_categorical(val_y)
        y_test = np_utils.to_categorical(test_y)
        x_train = x_train.astype('float32') / 255.0
        x_val = x_val.astype('float32') / 255.0
        x_test = x_test.astype('float32') / 255.0

        model = Sequential()
        model.add(Conv2D(filters=8, kernel_size=(5, 5), padding='same', input_shape=(imsize, imsize, 3), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Conv2D(filters=16, kernel_size=(3, 3), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Conv2D(filters=32, kernel_size=(3, 3), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Flatten())
        model.add(Dense(64, activation='relu'))
        model.add(Dense(32, activation='relu'))
        model.add(Dense(labelcount, activation='softmax'))

        callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=8, mode='min')
        model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.00003), metrics=['accuracy', sensitivity, precision, specificity, f1_score]) 

        epoch = 200
        batch_size = 32
        train_history=model.fit(x=x_train, y=y_train, validation_data = (x_val, y_val), epochs=epoch, batch_size=batch_size, verbose=1, callbacks=[callback])

        model.save('./model/Normal_CNN16_model.h5')
        model.save_weights('./model/Normal_CNN16_weight.h5')

        train_acc_list = []
        val_acc_list = []
        def show_train_history_acc(train_acc, val_acc):
            plt.figure(1)
            
            plt.plot(train_history.history[train_acc])            
            train_acc_list.extend(train_history.history[train_acc])
            plt.plot(train_history.history[val_acc])
            val_acc_list.extend(train_history.history[val_acc])
            
            plt.title('Learning curve')
            plt.ylabel('Accuracy')
            plt.xlabel('Epoch')
            
            plt.legend(['train', 'val'], loc='upper left')
            plt.savefig('./learning_curve/Normal_CNN16_acc.png')
        show_train_history_acc('accuracy', 'val_accuracy')

        train_loss_list = []
        val_loss_list = []
        def show_train_history_loss(train_loss, val_loss):
            plt.figure(2)
            
            plt.plot(train_history.history[train_loss])
            train_loss_list.extend(train_history.history[train_loss])
            plt.plot(train_history.history[val_loss])
            val_loss_list.extend(train_history.history[val_loss])
            
            plt.title('Learning curve')
            plt.ylabel('Loss')
            plt.xlabel('Epoch')
            
            plt.legend(['train', 'val'], loc='upper left')
            plt.savefig('./learning_curve/Normal_CNN16_loss.png')
        show_train_history_loss('loss', 'val_loss')
        
        # print("Accuracy =", val_acc_list[len(val_acc_list)-1])
        
        scores = model.evaluate(x_test, y_test, verbose=2)
        print("Test Accuracy = ", scores[1]) 
        # print("Test Loss: = ", scores[0])
        
        predict_classes = model.predict_classes(x_test)
        # print("Label = ", predict_classes)