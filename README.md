# Classifier and Attack Toolkit
此項目將提供分類器和攻擊工具

![QR code](/QRcode/QRcode.png)

分類模型
    
    1. CNN
    2. VGGNet
    3. Inception-v3
    4. ResNet
 
資料集 
    
    1. dataset 1: actual thermal image - 15 classes
    2. dataset 2: actual thermal image - 15+1(Others) classes
    3. dataset 3: actual thermal image - 15+1(DCGAN generate) classes
    4. dataset 4: actual thermal image - 30 classes
    5. dataset 5: actual thermal image - 30+1(Others) classes
    6. dataset 6: actual thermal image - 30+1(DCGAN generate) classes
    7. dataset 7: actual thermal image - 60 classes
    8. dataset 8: actual thermal image - 60+1(Others) classes
    9. dataset 9: actual thermal image - 60+1(DCGAN generate) classes
    10. dataset 10: simulated thermal image - 135 classes
    11. dataset 11: simulated thermal image - 135+1(Others) classes
    12. dataset 12: simulated thermal image - 135+1(DCGAN generate) classes
    13. dataset 13: simulated thermal image - 84 classes
    14. dataset 14: simulated thermal image - 84+1(Others) classes
    15. dataset 15: simulated thermal image - 84+1(DCGAN generate) classes
    16. dataset 16: simulated thermal image(DCGAN) - 84 classes
    17. dataset 17: simulated thermal image(DCGAN) - 84+1(Others) classes
    18. dataset 18: simulated thermal image(DCGAN) - 84+1(DCGAN generate) classes
    
攻擊工具
    
    1.C-DCGAN生成假圖像。(finished)
    2.進行攻擊。(Coming Soon)

環境
    
    1. Ubuntu 18.04.5 LTS
    2. NVIDIA Tesla V100
    3. GUDA 11.2

創建虛擬環境與安裝編譯器

    1. Anaconda安裝方法請參閱 Install Anaconda and Create Environment.pdf
    2. 創建環境與安裝套件指令: 
        conda create -n toolkit python=3.6 numpy=1.16 tensorflow-gpu=1.14 keras pandas opencv matplotlib spyder
        pip3 install torch==1.9.0+cu111 torchvision==0.10.0+cu111 torchaudio==0.9.0 -f https://download.pytorch.org/whl/torch_stable.html   
    3. 套件版本請參閱 requirements.txt

執行
    
    1. conda activate toolkit
    2. spyder
    3. run main.py

雲端連結
    
    Link : https://reurl.cc/bXY9dX
    - 資料集
    - 生成假圖的路徑
    - 儲存的模型與權重
    
資料夾階層  

    ├─ classifier
        ├─ original
        ├─ normal
        └─ defense
    ├─ attack
        ├─ original
        ├─ normal
        └─ defense
    ├─ model
        └─ generate_pth
    ├─ cdcgan_denerator
    ├─ generate_image
    ├─ generate_image_1pic
    ├─ label
    ├─ PPT
    ├─ dataset
        ├─ original_data
        ├─ normal_data
        ├─ defense_data
        └─ attack_data
    ├─ learning_curve
    ├─ main.py
    ├─ testmodel_choose.py
    ├─ attackmodel_choose.py
    ├─ load_attack_data.py
    └─ model.py


0729新增:生成攻擊熱像圖功能

1.依照選項選擇要攻擊的分類器&生成的張數。
2.生成的攻擊熱像圖會在./generate_image路徑下。


0926新增:新增計算攻擊成功率&防禦率功能

1.使用我們的熱像圖計算結果或自行輸入圖片計算結果，並依照選項選擇要攻擊的模型。
2.分類器預設為15類，如需使用其他的分類器請至attackmodel_choose.py(for我們的熱像圖)，attackmodel_choose2.py(for自行輸入的熱像圖)修改。
3.要輸入的攻擊照片 請放在指定路徑: "./dataset/input_attack_data/ (依照要攻擊的分類器&類別選擇資料夾) "

