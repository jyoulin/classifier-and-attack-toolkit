class Original_ResNet84_load(): 
    def __init__(self, x_attack, y_attack):
        self.original_resnet84_load(x_attack, y_attack)

    def original_resnet84_load(self, x_attack, y_attack):
        import warnings
        import os
        import numpy as np
        import tensorflow as tf
        from keras.models import load_model
        from keras.optimizers import Adam
        from keras.backend import round, clip, sum, epsilon

        warnings.filterwarnings("ignore")
        tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
        os.environ['PYTHONHASHSEED']='0'
        os.environ['CUDA_VISIBLE_DEVICES']='0'
        os.environ['TF_CUDNN_USE_AUTOTUNE'] ='0'
        os.environ['KMP_WARNINGS'] = 'off'
        cfg = tf.ConfigProto()
        cfg.gpu_options.allow_growth = True
        session = tf.Session(config=cfg)

        def cal_base(y_true, y_pred):
            y_pred_positive = round(clip(y_pred, 0, 1))
            y_pred_negative = 1 - y_pred_positive
            y_positive = round(clip(y_true, 0, 1))
            y_negative = 1 - y_positive
            TP = sum(y_positive * y_pred_positive)
            TN = sum(y_negative * y_pred_negative)
            FP = sum(y_negative * y_pred_positive)
            FN = sum(y_positive * y_pred_negative)
            return TP, TN, FP, FN

        def acc(y_true, y_pred):
            TP, TN, FP, FN = cal_base(y_true, y_pred)
            ACC = (TP + TN) / (TP + FP + FN + TN + epsilon())
            return ACC

        def sensitivity(y_true, y_pred):
            """ recall """
            TP, TN, FP, FN = cal_base(y_true, y_pred)
            SE = TP/(TP + FN + epsilon())
            return SE

        def precision(y_true, y_pred):
            TP, TN, FP, FN = cal_base(y_true, y_pred)
            PC = TP/(TP + FP + epsilon())
            return PC

        def specificity(y_true, y_pred):
            TP, TN, FP, FN = cal_base(y_true, y_pred)
            SP = TN / (TN + FP + epsilon())
            return SP

        def f1_score(y_true, y_pred):
            SE = sensitivity(y_true, y_pred)
            PC = precision(y_true, y_pred)
            F1 = 2 * SE * PC / (SE + PC + epsilon())
            return F1

        model = load_model('./model/Original_ResNet84_model.h5', custom_objects={'sensitivity': sensitivity, 'precision': precision, 'specificity': specificity, 'f1_score': f1_score}) 
        model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.00001), metrics=['accuracy', sensitivity, precision, specificity, f1_score]) 

        # scores = model.evaluate(x_attack, y_attack, verbose=2)
        # print("Test Accuracy = ", scores[1]) 
        # print("Test Loss: = ", scores[0])
        
        print("Original_ResNet_84")
        attack_data_path =  "./dataset/input_attack_data/84classes/"
        dirs = [d for d in os.listdir(attack_data_path) if os.path.isdir(os.path.join(attack_data_path,d))]
        num_dir = len(dirs)
        for i in dirs:
          pic_number = [f for f in os.listdir(attack_data_path+i) if os.path.isfile(os.path.join(attack_data_path+i,f))]
          num = len(pic_number)
        #attack label           
        predict = model.predict(x_attack)
        predict_classes = np.argmax(predict,axis=1)
        pic_class = np.reshape(predict_classes,(num,-1))
        pic_class = pic_class.tolist()
        from tkinter import _flatten
        pic_class = list(_flatten(pic_class))
        #print("Label = ", pic_class)
        
        #true_label
        true_label_all = []
        for i in range(0,num*num_dir):
          a = y_attack[i].astype(int)
          a = a.tolist()
          true_label = a.index(1)
          true_label = str(true_label)
          true_label_all.append(true_label)
        true_label_all = list(map(int, true_label_all))
        #print(pic_class)
        #print(true_label_all)
        count = 0
        print("-----attack-----")
        for k in range (0,num_dir): 
            single_count = 0
            single_catetory_true = true_label_all[k*num:(k+1)*num]
            single_catetory_predict = pic_class[k*num:(k+1)*num]
            for i in range(0,num):
                if(single_catetory_true[i]==single_catetory_predict[i]):   
                    count = count +1
                    single_count = single_count +1
            print("no", k+1 ," class's attack success rate is : ", (single_count/(num))*100 ,'%')
        print("total attack success rate is: ", count/(num*num_dir)*100 ,'%')