import os
import warnings
warnings.filterwarnings("ignore")

from load_attack_data import LoadAttackData

from attack.original.Original_CNN_15_load import Original_CNN15_load
from attack.original.Original_CNN_30_load import Original_CNN30_load
from attack.original.Original_CNN_60_load import Original_CNN60_load
from attack.original.Original_CNN_84_load import Original_CNN84_load
from attack.original.Original_CNN_84dcgan_load import Original_CNN84dcgan_load
from attack.original.Original_CNN_135_load import Original_CNN135_load
from attack.original.Original_VGGNet_15_load import Original_VGGNet15_load
from attack.original.Original_VGGNet_30_load import Original_VGGNet30_load
from attack.original.Original_VGGNet_60_load import Original_VGGNet60_load
from attack.original.Original_VGGNet_84_load import Original_VGGNet84_load
from attack.original.Original_VGGNet_84dcgan_load import Original_VGGNet84dcgan_load
from attack.original.Original_VGGNet_135_load import Original_VGGNet135_load
from attack.original.Original_Inception_15_load import Original_Inception15_load
from attack.original.Original_Inception_30_load import Original_Inception30_load
from attack.original.Original_Inception_60_load import Original_Inception60_load
from attack.original.Original_Inception_84_load import Original_Inception84_load
from attack.original.Original_Inception_84dcgan_load import Original_Inception84dcgan_load
from attack.original.Original_Inception_135_load import Original_Inception135_load
from attack.original.Original_ResNet_15_load import Original_ResNet15_load
from attack.original.Original_ResNet_30_load import Original_ResNet30_load
from attack.original.Original_ResNet_60_load import Original_ResNet60_load
from attack.original.Original_ResNet_84_load import Original_ResNet84_load
from attack.original.Original_ResNet_84dcgan_load import Original_ResNet84dcgan_load
from attack.original.Original_ResNet_135_load import Original_ResNet135_load

from attack.normal.Normal_CNN_16_load import Normal_CNN16_load
from attack.normal.Normal_CNN_31_load import Normal_CNN31_load
from attack.normal.Normal_CNN_61_load import Normal_CNN61_load
from attack.normal.Normal_CNN_85_load import Normal_CNN85_load
from attack.normal.Normal_CNN_85dcgan_load import Normal_CNN85dcgan_load
from attack.normal.Normal_CNN_136_load import Normal_CNN136_load
from attack.normal.Normal_VGGNet_16_load import Normal_VGGNet16_load
from attack.normal.Normal_VGGNet_31_load import Normal_VGGNet31_load
from attack.normal.Normal_VGGNet_61_load import Normal_VGGNet61_load
from attack.normal.Normal_VGGNet_85_load import Normal_VGGNet85_load
from attack.normal.Normal_VGGNet_85dcgan_load import Normal_VGGNet85dcgan_load
from attack.normal.Normal_VGGNet_136_load import Normal_VGGNet136_load
from attack.normal.Normal_Inception_16_load import Normal_Inception16_load
from attack.normal.Normal_Inception_31_load import Normal_Inception31_load
from attack.normal.Normal_Inception_61_load import Normal_Inception61_load
from attack.normal.Normal_Inception_85_load import Normal_Inception85_load
from attack.normal.Normal_Inception_85dcgan_load import Normal_Inception85dcgan_load
from attack.normal.Normal_Inception_136_load import Normal_Inception136_load
from attack.normal.Normal_ResNet_16_load import Normal_ResNet16_load
from attack.normal.Normal_ResNet_31_load import Normal_ResNet31_load
from attack.normal.Normal_ResNet_61_load import Normal_ResNet61_load
from attack.normal.Normal_ResNet_85_load import Normal_ResNet85_load
from attack.normal.Normal_ResNet_85dcgan_load import Normal_ResNet85dcgan_load
from attack.normal.Normal_ResNet_136_load import Normal_ResNet136_load

from attack.defense.Defense_CNN_16_load import Defense_CNN16_load
from attack.defense.Defense_CNN_31_load import Defense_CNN31_load
from attack.defense.Defense_CNN_61_load import Defense_CNN61_load
from attack.defense.Defense_CNN_85_load import Defense_CNN85_load
from attack.defense.Defense_CNN_85dcgan_load import Defense_CNN85dcgan_load
from attack.defense.Defense_CNN_136_load import Defense_CNN136_load
from attack.defense.Defense_VGGNet_16_load import Defense_VGGNet16_load
from attack.defense.Defense_VGGNet_31_load import Defense_VGGNet31_load
from attack.defense.Defense_VGGNet_61_load import Defense_VGGNet61_load
from attack.defense.Defense_VGGNet_85_load import Defense_VGGNet85_load
from attack.defense.Defense_VGGNet_85dcgan_load import Defense_VGGNet85dcgan_load
from attack.defense.Defense_VGGNet_136_load import Defense_VGGNet136_load
from attack.defense.Defense_Inception_16_load import Defense_Inception16_load
from attack.defense.Defense_Inception_31_load import Defense_Inception31_load
from attack.defense.Defense_Inception_61_load import Defense_Inception61_load
from attack.defense.Defense_Inception_85_load import Defense_Inception85_load
from attack.defense.Defense_Inception_85dcgan_load import Defense_Inception85dcgan_load
from attack.defense.Defense_Inception_136_load import Defense_Inception136_load
from attack.defense.Defense_ResNet_16_load import Defense_ResNet16_load
from attack.defense.Defense_ResNet_31_load import Defense_ResNet31_load
from attack.defense.Defense_ResNet_61_load import Defense_ResNet61_load
from attack.defense.Defense_ResNet_85_load import Defense_ResNet85_load
from attack.defense.Defense_ResNet_85dcgan_load import Defense_ResNet85dcgan_load
from attack.defense.Defense_ResNet_136_load import Defense_ResNet136_load

class AttackModel2():
    def __init__(self, action):
        self.__action = action
        self.attack_model(action)
        
    def attack_dataset_show(self, action):
        print("\nWe use the C-DCGAN thermal images to input each model and calculate the accuracy.")

    def model_show(self):
        print("\nThe target attack model has the following...")
        print("(1) Model 1: CNN")
        print("(2) Model 2: VGGNet")
        print("(3) Model 3: Inception-V3")
        print("(4) Model 4: ResNet")
    
    def attack_model(self, action):
        m_gate = 0
        while(m_gate == 0):
            self.model_show()
            model = input("Please choose a model(1~4): ")
            os.system('cls')
            if model == '1':
                cnn_d_gate = 0
                while(cnn_d_gate == 0):
                    self.attack_dataset_show(action)
                    print("load data and model to calculate accuracy")
                    x_attack, y_attack = LoadAttackData().load_15_attack_data()
                    Original_CNN15_load(x_attack, y_attack)
                    Normal_CNN16_load(x_attack, y_attack)
                    Defense_CNN16_load(x_attack, y_attack)
                    #x_attack, y_attack = LoadAttackData().load_30_attack_data()
                    #Original_CNN30_load(x_attack, y_attack)
                    #Normal_CNN31_load(x_attack, y_attack)
                    #Defense_CNN31_load(x_attack, y_attack)
                    #x_attack, y_attack = LoadAttackData().load_60_attack_data()
                    #Original_CNN60_load(x_attack, y_attack)
                    #Normal_CNN61_load(x_attack, y_attack)
                    #Defense_CNN61_load(x_attack, y_attack)
                    #x_attack, y_attack = LoadAttackData().load_84_attack_data(model)
                    #Original_CNN84_load(x_attack, y_attack)
                    #Normal_CNN85_load(x_attack, y_attack)
                    #Defense_CNN85_load(x_attack, y_attack)
                    #x_attack, y_attack = LoadAttackData().load_84dcgan_attack_data(model)
                    #Original_CNN84dcgan_load(x_attack, y_attack)
                    #Normal_CNN85dcgan_load(x_attack, y_attack)
                    #Defense_CNN85dcgan_load(x_attack, y_attack)
                    #x_attack, y_attack = LoadAttackData().load_135_attack_data(model)
                    #Original_CNN135_load(x_attack, y_attack)
                    #Normal_CNN136_load(x_attack, y_attack)
                    #Defense_CNN136_load(x_attack, y_attack)
                    cnn_d_gate = 1
                m_gate = 1
                os.system('pause')
                          
            elif model == '2':
                vgg_d_gate = 0
                while(vgg_d_gate == 0):
                    self.attack_dataset_show(action)
                    print("load model and calculate accuracy")
                    x_attack, y_attack = LoadAttackData().load_15_attack_data()
                    Original_VGGNet15_load(x_attack, y_attack)
                    Normal_VGGNet16_load(x_attack, y_attack)
                    Defense_VGGNet16_load(x_attack, y_attack)
                    #x_attack, y_attack = LoadAttackData().load_30_attack_data()
                    #Original_VGGNet30_load(x_attack, y_attack)
                    #Normal_VGGNet31_load(x_attack, y_attack)
                    #Defense_VGGNet31_load(x_attack, y_attack)
                    #x_attack, y_attack = LoadAttackData().load_60_attack_data()
                    #Original_VGGNet60_load(x_attack, y_attack)
                    #Normal_VGGNet61_load(x_attack, y_attack)
                    #Defense_VGGNet61_load(x_attack, y_attack)
                    #x_attack, y_attack = LoadAttackData().load_84_attack_data(model)
                    #Original_VGGNet84_load(x_attack, y_attack)
                    #Normal_VGGNet85_load(x_attack, y_attack)
                    #Defense_VGGNet85_load(x_attack, y_attack)
                    #x_attack, y_attack = LoadAttackData().load_84dcgan_attack_data(model)
                    #Original_VGGNet84dcgan_load(x_attack, y_attack)
                    #Normal_VGGNet85dcgan_load(x_attack, y_attack)
                    #Defense_VGGNet85dcgan_load(x_attack, y_attack)
                    #x_attack, y_attack = LoadAttackData().load_135_attack_data(model)
                    #Original_VGGNet135_load(x_attack, y_attack)
                    #Normal_VGGNet136_load(x_attack, y_attack)
                    #Defense_VGGNet136_load(x_attack, y_attack)
                    vgg_d_gate = 1
                m_gate = 1
                os.system('pause')
                
            elif model == '3':
                inception_d_gate = 0
                while(inception_d_gate == 0):
                    self.attack_dataset_show(action)
                    print("load model and calculate accuracy")
                    x_attack, y_attack = LoadAttackData().load_15_attack_data()
                    Original_Inception15_load(x_attack, y_attack)
                    Normal_Inception16_load(x_attack, y_attack)
                    Defense_Inception16_load(x_attack, y_attack)
                    #x_attack, y_attack = LoadAttackData().load_30_attack_data()
                    #Original_Inception30_load(x_attack, y_attack)
                    #Normal_Inception31_load(x_attack, y_attack)
                    #Defense_Inception31_load(x_attack, y_attack)
                    #x_attack, y_attack = LoadAttackData().load_60_attack_data()
                    #Original_Inception60_load(x_attack, y_attack)
                    #Normal_Inception61_load(x_attack, y_attack)
                    #Defense_Inception61_load(x_attack, y_attack)
                    #x_attack, y_attack = LoadAttackData().load_84_attack_data(model)
                    #Original_Inception84_load(x_attack, y_attack)
                    #Normal_Inception85_load(x_attack, y_attack)
                    #Defense_Inception85_load(x_attack, y_attack)
                    #x_attack, y_attack = LoadAttackData().load_84dcgan_attack_data(model)
                    #Original_Inception84dcgan_load(x_attack, y_attack)
                    #Normal_Inception85dcgan_load(x_attack, y_attack)
                    #Defense_Inception85dcgan_load(x_attack, y_attack)
                    #x_attack, y_attack = LoadAttackData().load_135_attack_data(model)
                    #Original_Inception135_load(x_attack, y_attack)
                    #Normal_Inception136_load(x_attack, y_attack)
                    #Defense_Inception136_load(x_attack, y_attack)
                    inception_d_gate = 1
                m_gate = 1
                os.system('pause')
                
            elif model == '4':
                resnet_d_gate = 0
                while(resnet_d_gate == 0):
                    self.attack_dataset_show(action)
                    print("load model and calculate accuracy")
                    x_attack, y_attack = LoadAttackData().load_15_attack_data()
                    Original_ResNet15_load(x_attack, y_attack)
                    Normal_ResNet16_load(x_attack, y_attack)
                    Defense_ResNet16_load(x_attack, y_attack)
                    #x_attack, y_attack = LoadAttackData().load_30_attack_data()
                    #Original_ResNet30_load(x_attack, y_attack)
                    #Normal_ResNet31_load(x_attack, y_attack)
                    #Defense_ResNet31_load(x_attack, y_attack)
                    #x_attack, y_attack = LoadAttackData().load_60_attack_data()
                    #Original_ResNet60_load(x_attack, y_attack)
                    #Normal_ResNet61_load(x_attack, y_attack)
                    #Defense_ResNet61_load(x_attack, y_attack)
                    #x_attack, y_attack = LoadAttackData().load_84_attack_data(model)
                    #Original_ResNet84_load(x_attack, y_attack)
                    #Normal_ResNet85_load(x_attack, y_attack)
                    #Defense_ResNet85_load(x_attack, y_attack)
                    #x_attack, y_attack = LoadAttackData().load_84dcgan_attack_data(model)
                    #Original_ResNet84dcgan_load(x_attack, y_attack)
                    #Normal_ResNet85dcgan_load(x_attack, y_attack)
                    #Defense_ResNet85dcgan_load(x_attack, y_attack)
                    #x_attack, y_attack = LoadAttackData().load_135_attack_data(model)
                    #Original_ResNet135_load(x_attack, y_attack)
                    #Normal_ResNet136_load(x_attack, y_attack)
                    #Defense_ResNet136_load(x_attack, y_attack)
                    resnet_d_gate = 1
                m_gate = 1
                os.system('pause')